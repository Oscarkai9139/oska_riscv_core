#include "stdlib.h"


// void mul(int* r, int a, int b);
void mul(long long *r, int a, int b);
extern long long _test_start;
int addr = 0x8000;
int main(){

    // ======================== with libgcc.a ===========================
    /*
    extern const int mul1;
    extern const int mul2;
    extern long long _test_start;


    long long result;
    long long data1;
    long long data2;

    data1 = mul1;
    data2 = mul2;


    result = data1 * data2;
    _test_start = result;
    */
    // ====================================================================

    // ========================= w/o libgcc.a =============================
    extern const int mul1;
    extern const int mul2;
    

    int data1;
    int data2;
    long long result;

     data1 = mul2;
     data2 = mul1;


     mul(&result , data1, data2);
    //  result = data1 * data2;
    // result = (data1 * data2);
    _test_start = result;
    return 0;
}


void mul(long long *r, int a, int b){
    // r -> a0, a -> a1, b -> a2    
    __asm__ __volatile__("srli t1, a1, 31;");// a1 signed bit
    __asm__ __volatile__("srli a5, a2, 31;");// a2 signed bit 
    __asm__ __volatile__("sub t0, zero, t1;");
    __asm__ __volatile__("li t4, 0;");
    __asm__ __volatile__("li t2, 0;");
    __asm__ __volatile__(
                        "MUL:"
                        "andi t3, a2, 1;"
    );
    __asm__ __volatile__("beqz t3, MUL+0x24;"); //Notice the branch address
    __asm__ __volatile__("mv t5, t2;");
    __asm__ __volatile__("add t2, t2, a1;"); 
    __asm__ __volatile__("sltu t5, t2, t3;");
    __asm__ __volatile__("sltu t6, t2, a1;");
    __asm__ __volatile__("or t6, t5, t6;");
    __asm__ __volatile__("add t4, t4, t6;");
    __asm__ __volatile__("add t4, t4, t0;");
    __asm__ __volatile__(
                        "srli a2, a2, 1;"
    );
    __asm__ __volatile__("srli t3, a1, 31;");
    __asm__ __volatile__("slli t0, t0, 1;");
    __asm__ __volatile__("or t0, t0, t3;");
    __asm__ __volatile__("slli a1, a1, 1;");
    __asm__ __volatile__("bnez a2, MUL;");
    
    // support neg b
    __asm__ __volatile__("beqz a5, DONE;");
    __asm__ __volatile__("li a5, 0;");
    __asm__ __volatile__("li a2, 0xffffffff;");
    __asm__ __volatile__("j MUL;");
    //

    __asm__ __volatile__("DONE: sw t2, 0(a0);");
    __asm__ __volatile__("sw t4, 4(a0);");

    // __asm__ __volatile__("sw t2, 0(%[out]);" :: [out] "r" (addr));
    // __asm__ __volatile__("sw t4, 4(%[out]);" :: [out] "r" (addr));
}