

void mod(unsigned int *r, unsigned int a, unsigned int b);
int main(){
    extern unsigned const int div1;
    extern unsigned const int div2;
    extern unsigned int _test_start;

    unsigned int a = div1;
    unsigned int b = div2;

    while(a!=0 && b !=0){
        if(a >= b){
            // a = a % b;
            mod(&a, a, b);
        }
        else{
            // b = b % a;
            mod(&b, b, a);
        }
    }

    if(a >= b){
        _test_start = a;
    }
    else {
        _test_start = b;
    }

    return 0;

}

void mod(unsigned int *r, unsigned int a, unsigned int b){
    __asm__("MOD: bltu a1, a2, MOD+0xc;");
    __asm__("sub a1, a1, a2;");
    __asm__("j MOD;");
    __asm__("sw a1, 0(a0);");
}