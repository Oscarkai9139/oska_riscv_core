# Oska RISC-V Core
## Project Summary
This is a RTL level designs of a 5-stage pipeline RISC-V processor, which is written in SystemVerilog and currently support RV32I Instruction set. The architecture of Oska RISC-V Core is inspired by the classic design of MIPS32 processor which is introduced by John L. Hennessy & David A. Patterson in Computer Architecture Textbook. 

## Source Code file hierarchy Desciption
```
N26095025/
+-- sim/
    +-- prog1/
        +-- prog1.c
    +-- prog2/
        +-- prog2.c
    +-- prog3/
        +-- prog3.c
+-- src/
    +-- oska_define.sv
    +-- top.sv
    +-- oska_cpu.sv
    +-- oska_ifu.sv
    +-- oska_idu.sv
    +-- oska_exu.sv
    +-- oska_mem.sv
    +-- oska_wbu.sv
    +-- oska_register_file.sv
    +-- oska_pc.sv
```

Oska RISC-V Core ```oska_cpu.sv``` will connect with one instruction memory & one data memory in ```top.sv```, and ```oska_cpu.sv``` connects all five stages include ```oska_ifu.sv```, ```oska_idu.sv```, ```oska_exu.sv```, ```oska_mem.sv```, ```oska_wbu.sv```. For submodules, ```oska_ifu.sv``` includes a program counter ```oska_pc.sv```, ```oska_idu.sv``` includes a resgister file ```oska_register_file.sv```. The hazards detection & forwarding unit for Oska RISC-V Core are directly implement in these 5 stages, aims to simplify the designs and avoid huge amount of interconnections between submodule. 

### Variable Naming Rules
All common definations are written in ```oska_define.sv```, and only defined terms are using the capital letters, example ``` `WORD_SIZE```, ``` `ALUOP_ADD``` and so on.
The naming of all wires in this project is strictly follow the rules bellow:
* ```[DIRECTION]_[PIPELINE REG]_[NAME]_[DESCRIPTION]s```
    * example: ```idu2exu_pr_rs1_id```
* ```[DIRECTION]_[FWD]_[NAME]_[DESCRIPTON]s```
    * example: ```exu2mem_fwd_reg_data```
* ```[SUBMODULE]_[NAME]_[DESCRIPTION]s```
    * example: ```alu_add_result```, ```idu_aluop_hold```

## Introduction to Oska RISC-V Core 
![](https://i.imgur.com/Anb9aWM.png)
This picture is just a temporary architecture designs of Oska Core, and i will update it later.

The behavior of Oska Core when facing these problems:
* wbu-idu hazard
    * hazard detect at idu stage, when idu stage want to write back a register that needed to read at the same time, write data is forward to exu on the next cycle
* mem-exu hazard
    * when two consecutive instruction have data dependencies, hazard can be solve by forwarding the result of mem stage to exu stage.
* exu-idu hazard
    * Oska core solve this hazard at idu stage, and write back data is forward o exu on the next cycle
* branch taken
    * flush signal to ifu & idu
    * pc change to jump address
    * but at the same time, an invalid instruction signal must be added in next cycle for idu stage
    * As the result, when branch taken oska core flush the previos two instructions
* load memory taken
    * stall signal to ifu
    * pc is stall for one cycle

## RISC-V Instruction supports
* Oska RISC-V Core supports RV32I Instruction set which includes:
![](https://i.imgur.com/h5DiEOa.png)
![](https://i.imgur.com/X2X0yf3.png)
![](https://i.imgur.com/M8zr4N0.png)
![](https://i.imgur.com/NCkW42o.png)
![](https://i.imgur.com/5cqjQOe.png)
![](https://i.imgur.com/gPICBg6.png)

## Waveform Explanations
### rtype
### itype
### stype
### btype
### jtype
### utype

## SuperLint Problems encountered
### First make superlint result
![](https://i.imgur.com/CwdWRcT.png)
When first ```make superlint```, i have total 67+9+1=77 violation messages 
* Warning messages
![](https://i.imgur.com/AL2Nq2Z.png)
* Assertion summary
![](https://i.imgur.com/eLgIuAZ.png)

### Problems 1
![](https://i.imgur.com/rK8AhZl.png)
*　Add default case for the case statement
![](https://i.imgur.com/5zSYhJi.png)
### Problems 2
![](https://i.imgur.com/QKlmVsE.png)
* make the operand equal length as the another operand
![](https://i.imgur.com/zvrev2W.png)
### Problems 3
![](https://i.imgur.com/Uwbsxfn.png)
*　imask_aluop size was wrong declared, solved by update the size of the imask_aluop
![](https://i.imgur.com/7NsfbA0.png)

### Final results
![](https://i.imgur.com/74ZIw71.png)
![](https://i.imgur.com/KK58XCg.png)

* At the end, i get only 47 violation messages which has reached a quite well code coverage 97.07%. 
    * Violation messages: 47
    * Source code total lines: 1603
    * Code coverage = 97.07

## Conclusion
I learned a lot when try find the problems in my cpu, and how to solve the problem in correct way. superlint also help me to write SystemVerilog in more correct way.