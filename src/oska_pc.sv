`include "oska_define.sv"

module oska_pc(input  logic clk,
               input  logic rst,

               // Stall signal from wrapper
               input  logic                wrp2cpu_stall,

               // Stall signal from idu when jump instaruction
               input  logic                idu2pc_stall,
               input  logic                exu2pc_jump,
               input  logic [`PC_SIZE-1:0] exu2pc_jump_addr,

               output logic [`PC_SIZE-1:0] pc2ifu_pc_next
             );

  // Wires
  logic [`PC_SIZE-1:0] pc_next_hold;
  logic [`PC_SIZE-1:0] pc_current;
  logic [`PC_SIZE-1:0] pc_next;
  logic [`PC_SIZE-1:0] pc_jump;

  // Regs
  logic [`PC_SIZE-1:0] pc;


  // PC instruction calculations
  assign pc_current = pc;
  assign pc_next    = pc + `PC_SIZE'b100;
  assign pc_jump    = exu2pc_jump_addr;

  // Output PC assignment
  assign pc2ifu_pc_next  = pc;

  // PC jump select
  assign pc_next_hold = (exu2pc_jump)? exu2pc_jump_addr + `PC_SIZE'b100:
                        (idu2pc_stall)? pc_current: pc_next;

  // pipeline register
  always_ff @(posedge clk, posedge rst)
  begin
    if( rst ) begin
      pc      <= `PC_SIZE'b0;
    end
    else if (wrp2cpu_stall) begin
      pc <= (exu2pc_jump)? exu2pc_jump_addr: pc;
    end
    else begin
      pc <= pc_next_hold;
    end
  end

endmodule
