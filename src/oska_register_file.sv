`include "oska_define.sv"

module oska_register_file(input logic clk,
                          input logic rst,

                          //input logic wbu2rf_byte,

                          input  logic [`ADDR_SIZE-1:0] reg1_raddr,
                          output logic [`WORD_SIZE-1:0] reg1_rdata,

                          input  logic [`ADDR_SIZE-1:0] reg2_raddr,
                          output logic [`WORD_SIZE-1:0] reg2_rdata,

                          input logic                  reg1_web,
                          input logic [`ADDR_SIZE-1:0] reg1_waddr,
                          input logic [`WORD_SIZE-1:0] reg1_wdata);

  logic [`WORD_SIZE-1:0] oska_reg[`REG_SIZE];
  logic [`WORD_SIZE-1:0] reg1_wdata_hold;

  assign reg1_rdata = oska_reg[reg1_raddr];
  assign reg2_rdata = oska_reg[reg2_raddr];


  always_comb
  begin
    case(reg1_waddr)
      `ADDR_SIZE'b0: reg1_wdata_hold = `WORD_SIZE'b0;
        default    : reg1_wdata_hold = reg1_wdata;
    endcase
  end

  // Write port
  always_ff @(posedge clk, posedge rst)
  if (rst) begin
    oska_reg[0] <= `WORD_SIZE'b0;
  end
  else begin: register_file_write_port
    if (reg1_web) begin
        oska_reg[reg1_waddr] <= reg1_wdata_hold;
    end
  end

endmodule
