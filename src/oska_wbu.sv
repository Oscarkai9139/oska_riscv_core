`include "oska_define.sv"

module oska_wbu(

                input logic [`PC_SIZE-1:0]      mem2wbu_pr_pc,
                input logic [`REG_DATASIZE-1:0] mem2wbu_pr_reg_data,
                input logic                     mem2wbu_pr_mem2reg,
                input logic [`WORD_SIZE-1:0]    mem2wbu_mem_data,
                input logic                     mem2wbu_pr_byte,
                input logic                     mem2wbu_pr_half,
                input logic                     mem2wbu_pr_unsigned,

                output logic [`WORD_SIZE-1:0] wbu2rf_reg_data);


  logic [`WORD_SIZE-1:0] mem_data;
  logic [`WORD_SIZE-1:0] mem_data_byte;
  logic [`WORD_SIZE-1:0] mem_data_byte_u;
  logic [`WORD_SIZE-1:0] mem_data_half;
  logic [`WORD_SIZE-1:0] mem_data_half_u;

  assign mem_data_byte   = {{24{mem2wbu_mem_data[7]}}, mem2wbu_mem_data[7:0]};
  assign mem_data_byte_u = {24'b0, mem2wbu_mem_data[7:0]};
  assign mem_data_half   = {{16{mem2wbu_mem_data[15]}}, mem2wbu_mem_data[15:0]};
  assign mem_data_half_u = {16'b0, mem2wbu_mem_data[15:0]};

  always_comb begin
    case({mem2wbu_pr_byte, mem2wbu_pr_half, mem2wbu_pr_unsigned})
      3'b100:  mem_data = mem_data_byte;
      3'b101:  mem_data = mem_data_byte_u;
      3'b010:  mem_data = mem_data_half;
      3'b011:  mem_data = mem_data_half_u;
      default: mem_data = mem2wbu_mem_data;
    endcase
  end

  assign wbu2rf_reg_data = (mem2wbu_pr_mem2reg == 1'b0)?
    mem2wbu_pr_reg_data: mem_data;



endmodule
