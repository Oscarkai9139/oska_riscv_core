`define WORD_SIZE 32
`define BYTE_SIZE 8
`define ADDR_SIZE 5
`define REG_SIZE  32
`define REG_DATASIZE  `WORD_SIZE
`define REG_READ   1'b0
`define REG_WRITE  1'b1

`define TRUE  1'b1
`define FALSE 1'b0

// Instruction validity
`define VALID   1'b1
`define INVALID 1'b0

// Instruction Memory Macros
`define IM_ADDR_SIZE 14
`define IM_WORD_SIZE `WORD_SIZE
`define IM_WEB_SIZE  4
`define ENABLE  1'b1
`define DISABLE 1'b0
`define MEM_WRITE      4'b0000
`define MEM_HALF_WRITE 4'b1100
`define MEM_BYTE_WRITE 4'b1110
`define MEM_READ       4'b1111
`define MEM_HALF_READ  4'b0011
`define MEM_BYTE_READ  4'b0001

// Data Memory Macros
`define DM_ADDR_SIZE 14
`define DM_WORD_SIZE `WORD_SIZE
`define DM_WEB_SIZE  4

// ID Instruction mask
`define IMME_SIGN  31
`define SHAMT_SIZE 5
`define ALUOP_SIZE 3

// ID unit
`define OP_SIZE        7
`define OP_RTYPE       7'b0110011
`define OP_ITYPE_LOAD  7'b0000011
`define OP_ITYPE_ALU   7'b0010011
`define OP_ITYPE_JUMP  7'b1100111
`define OP_STYPE       7'b0100011
`define OP_BTYPE       7'b1100011
`define OP_UTYPE_AUIPC 7'b0010111
`define OP_UTYPE_LUI   7'b0110111
`define OP_JTYPE       7'b1101111

// OPtype
`define RTYPE       4'b0000
`define ITYPE_LOAD  4'b0001
`define ITYPE_ALU   4'b0010
`define ITYPE_JUMP  4'b0011
`define STYPE       4'b0100
`define BTYPE       4'b0101
`define UTYPE_AUIPC 4'b0110
`define UTYPE_LUI   4'b0111
`define JTYPE       4'b1000
`define ERROR_TYPE  4'b1111

`define ALUOP_ADD  3'b000
`define ALUOP_SUB  3'b000
`define ALUOP_SLL  3'b001
`define ALUOP_SLT  3'b010
`define ALUOP_SLTU 3'b011
`define ALUOP_XOR  3'b100
`define ALUOP_SRL  3'b101
`define ALUOP_SRA  3'b101
`define ALUOP_OR   3'b110
`define ALUOP_AND  3'b111

`define BOP_BEQ  3'b000
`define BOP_BNE  3'b001
`define BOP_BLT  3'b100
`define BOP_BGE  3'b101
`define BOP_BLTU 3'b110
`define BOP_BGEU 3'b111

`define REG_X0  5'b00000

`define PC_SIZE  14
`define PC_NEXT  2'b00
`define PC_JUMP  2'b01
`define PC_STALL 2'b10

// AXI CPU wrapper
// Instruction memory state machine
`define M0_IDLE 2'b00
`define M0_ADDR 2'b01
`define M0_DATA 2'b10
`define M0_WAIT 2'b11

// Data memory state machine
`define M1_IDLE  3'b000
`define M1_ADDR  3'b001
`define M1_DATA  3'b010
`define M1_WADDR 3'b011
`define M1_WDATA 3'b100
`define M1_WRESP 3'b101
`define M1_WAIT  3'b110

// Instruction Cache state machine
`define IMC_IDLE 2'b00
`define IMC_REQ  2'b01
`define IMC_MISS 2'b10
`define IMC_WAIT 2'b11

// Memory Cache state machine
`define DMC_IDLE 2'b00
`define DMC_REQ  2'b01
`define DMC_MISS 2'b10
`define DMC_WAIT 2'b11

// MASTER & SLAVE ID
`define M1_ID_WADDR 4'b0001
`define M1_ID_WRESP 4'b0001
`define M1_ID_RADDR 4'b0001
`define M1_ID_RDATA 4'b0001

`define M0_ID_RADDR 4'b0000
`define M0_ID_RDATA 4'b0000


