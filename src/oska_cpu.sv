`include "oska_define.sv"
`include "def.svh"
`include "oska_ifu.sv"
`include "oska_idu.sv"
`include "oska_exu.sv"
`include "oska_mem.sv"
`include "oska_wbu.sv"

module oska_cpu(
  input logic clk,
  input logic rst,

  // CPU Wrapper to OSKA cpu stall signals
  input logic                wrp2cpu_stall,
  input logic [`PC_SIZE-1:0] wrp2cpu_pc,
  input logic                wrp2cpu_rready,

  output logic [`CACHE_TYPE_BITS-1:0] cpu2dmc_type,

  // Data memory
  output logic                     cpu2dm1_cs,
  output logic                     cpu2dm1_oe,
  output logic [ `DM_WEB_SIZE-1:0] cpu2dm1_web,
  output logic [`WORD_SIZE-1:0]    cpu2dm1_addr,
  output logic [`DM_WORD_SIZE-1:0] cpu2dm1_di,
  input  logic [`DM_WORD_SIZE-1:0] dm12cpu_do,

  // Ins logictruction memory
  output logic                     ifu2wrp_cs,
  output logic                     ifu2wrp_oe,
  output logic [ `IM_WEB_SIZE-1:0] ifu2wrp_web,
  output logic [`WORD_SIZE-1:0]    cpu2im1_addr,
  output logic [`IM_WORD_SIZE-1:0] cpu2im1_di,
  input  logic [`IM_WORD_SIZE-1:0] im12cpu_do); // instr fetch

  // Pipeline registers wires
  logic [   `PC_SIZE-1:0] ifu2idu_pr_pc;
  logic [   `PC_SIZE-1:0] ifu2im1_pc_next;
  logic [ `WORD_SIZE-1:0] ifu2idu_pr_instr;
  logic                   ifu2idu_pr_instr_valid;
  logic [ `WORD_SIZE-1:0] im12ifu_instr;

  logic [   `PC_SIZE-1:0]  idu2exu_pr_pc     ;
  logic                    idu2exu_pr_byte   ;
  logic                    idu2exu_pr_half   ;
  logic                    idu2exu_pr_unsigned;
  logic [ `WORD_SIZE-1:0]  idu2exu_pr_imme   ;
  logic                    idu2exu_pr_sign   ;
  logic [ `ADDR_SIZE-1:0]  idu2exu_pr_rs1_id ;
  logic [ `ADDR_SIZE-1:0]  idu2exu_pr_rs2_id ;
  logic [ `WORD_SIZE-1:0]  idu2exu_pr_rs1    ;
  logic [ `WORD_SIZE-1:0]  idu2exu_pr_rs2    ;
  logic [`ALUOP_SIZE-1:0]  idu2exu_pr_aluop  ;
  logic [   `OP_SIZE-1:0]  idu2exu_pr_op     ;
  logic [3:0]              idu2exu_pr_optype ;
  logic [ `ADDR_SIZE-1:0]  idu2exu_pr_rd_addr;
  logic [`DM_WEB_SIZE-1:0] idu2exu_pr_mem_web;
  logic                    idu2exu_pr_mem_oe ;
  logic                    idu2exu_pr_reg_web;
  logic                    idu2exu_pr_mem2reg;
  logic                    idu2exu_pr_instr_valid;
  logic                    idu2exu_pr_flush  ;

  logic                    idu2pc_stall;
  logic                    idu2exu_wbu_fwd_reg_sel1;
  logic                    idu2exu_wbu_fwd_reg_sel2;

  logic [   `PC_SIZE-1:0]   exu2mem_pr_pc      ;
  logic                     exu2mem_pr_byte    ;
  logic                     exu2mem_pr_half    ;
  logic                     exu2mem_pr_unsigned;
  logic [ `ADDR_SIZE-1:0]   exu2mem_rs1_id     ;
  logic [ `ADDR_SIZE-1:0]   exu2mem_rs2_id     ;
  logic                     exu2mem_pr_mem_oe  ;
  logic [`DM_WEB_SIZE-1:0]  exu2mem_pr_mem_web ;
  logic [`WORD_SIZE-1:0]    exu2mem_pr_mem_addr;
  logic [`WORD_SIZE-1:0]    exu2mem_pr_mem_data;
  logic                     exu2mem_pr_reg_web ;
  logic [`ADDR_SIZE-1:0]    exu2mem_pr_reg_addr;
  logic [`WORD_SIZE-1:0]    exu2mem_pr_reg_data;
  logic                     exu2mem_pr_mem2reg ;

  logic                exu2pc_jump     ;
  logic [`PC_SIZE-1:0] exu2pc_jump_addr;

  logic                     mem2wbu_pr_byte    ;
  logic                     mem2wbu_pr_half    ;
  logic                     mem2wbu_pr_unsigned;
  logic [   `PC_SIZE-1:0]   mem2wbu_pr_pc      ;
  logic [ `ADDR_SIZE-1:0]   mem2wbu_pr_rs1_id  ;
  logic [ `ADDR_SIZE-1:0]   mem2wbu_pr_rs2_id  ;
  logic [`REG_DATASIZE-1:0] mem2wbu_pr_reg_data;
  logic                     mem2wbu_pr_mem2reg ;

  logic                  mem2idu_fwd_reg_web ;
  logic [`ADDR_SIZE-1:0] mem2idu_fwd_reg_addr;

  logic                     mem2exu_fwd_reg_sel1;
  logic                     mem2exu_fwd_reg_sel2;
  logic [`REG_DATASIZE-1:0] mem2exu_fwd_reg_data;

  logic                     wbu2rf_reg_web ;
  logic [`ADDR_SIZE-1:0]    wbu2rf_reg_addr;
  logic [`REG_DATASIZE-1:0] wbu2rf_reg_data;

  // CPU to CPU wrapper
  assign cpu2wrp_jump      = exu2pc_jump;
  assign cpu2wrp_jump_addr = exu2pc_jump_addr;

  // Intruction memory control signal
  assign cpu2im1_di  = `WORD_SIZE'b0;

  // Data memory control signal
  assign cpu2dm1_cs   = `ENABLE;
  assign cpu2dm1_oe   = exu2mem_pr_mem_oe;
  assign cpu2dm1_web  = exu2mem_pr_mem_web;
  assign cpu2dm1_addr = exu2mem_pr_mem_addr;
  assign cpu2dm1_di   = exu2mem_pr_mem_data;

  // Output assignment
  assign cpu2im1_addr = {18'b0, ifu2im1_pc_next};

  oska_ifu OSKA_IFU(.clk(clk),
                    .rst(rst),

                    .ifu2wrp_cs             (ifu2wrp_cs      ),
                    .ifu2wrp_oe             (ifu2wrp_oe      ),
                    .ifu2wrp_web            (ifu2wrp_web     ),


                    .wrp2cpu_pc             (wrp2cpu_pc      ),
                    .wrp2ifu_instr          (im12cpu_do      ),
                    .wrp2cpu_rready         (wrp2cpu_rready  ),
                    .wrp2cpu_stall          (wrp2cpu_stall   ),

                    .idu2pc_stall           (idu2pc_stall    ),
                    .exu2pc_jump            (exu2pc_jump     ),
                    .exu2pc_jump_addr       (exu2pc_jump_addr),
                    .ifu2im1_pc_next        (ifu2im1_pc_next ),

                    .ifu2idu_pr_pc          (ifu2idu_pr_pc   ),
                    .ifu2idu_pr_instr       (ifu2idu_pr_instr),
                    .ifu2idu_pr_instr_valid (ifu2idu_pr_instr_valid)
                   );

  oska_idu OSKA_IDU(.clk(clk),
                    .rst(rst),
                    .ctrl_flush_idu (exu2pc_jump),

                    .wrp2cpu_stall          (wrp2cpu_stall),

                    .ifu2idu_pr_instr       (ifu2idu_pr_instr),
                    .ifu2idu_pr_instr_valid (ifu2idu_pr_instr_valid),
                    .ifu2idu_pr_pc          (ifu2idu_pr_pc),
                    .idu2pc_stall           (idu2pc_stall),

                    .wbu2rf_reg_web  (wbu2rf_reg_web ),
                    .wbu2rf_reg_addr (wbu2rf_reg_addr),
                    .wbu2rf_reg_data (wbu2rf_reg_data),

                    .idu2exu_pr_instr_valid(idu2exu_pr_instr_valid),
                    .idu2exu_pr_pc      (idu2exu_pr_pc      ),
                    .idu2exu_pr_byte    (idu2exu_pr_byte    ),
                    .idu2exu_pr_half    (idu2exu_pr_half    ),
                    .idu2exu_pr_unsigned(idu2exu_pr_unsigned),
                    .idu2exu_pr_imme    (idu2exu_pr_imme    ),
                    .idu2exu_pr_sign    (idu2exu_pr_sign    ),
                    .idu2exu_pr_rs1_id  (idu2exu_pr_rs1_id  ),
                    .idu2exu_pr_rs2_id  (idu2exu_pr_rs2_id  ),
                    .idu2exu_pr_rs1     (idu2exu_pr_rs1     ),
                    .idu2exu_pr_rs2     (idu2exu_pr_rs2     ),
                    .idu2exu_pr_aluop   (idu2exu_pr_aluop   ),
                    .idu2exu_pr_optype  (idu2exu_pr_optype  ),
                    .idu2exu_pr_rd_addr (idu2exu_pr_rd_addr ),
                    .idu2exu_pr_flush   (idu2exu_pr_flush   ),

                    .mem2idu_reg_addr         (mem2idu_fwd_reg_addr),
                    .mem2idu_reg_web          (mem2idu_fwd_reg_web ),
                    .idu2exu_wbu_fwd_reg_sel1 (idu2exu_wbu_fwd_reg_sel1),
                    .idu2exu_wbu_fwd_reg_sel2 (idu2exu_wbu_fwd_reg_sel2),

                    .idu2exu_pr_mem_web (idu2exu_pr_mem_web),
                    .idu2exu_pr_mem_oe  (idu2exu_pr_mem_oe ),
                    .idu2exu_pr_reg_web (idu2exu_pr_reg_web),
                    .idu2exu_pr_mem2reg (idu2exu_pr_mem2reg)
                   );

  oska_exu OSKA_EXU(.clk(clk),
                    .rst(rst),

                    .wrp2cpu_stall    (wrp2cpu_stall     ),

                    .idu2exu_pr_instr_valid(idu2exu_pr_instr_valid),
                    .idu2exu_pr_flush   (idu2exu_pr_flush   ),
                    .idu2exu_pr_pc      (idu2exu_pr_pc      ),
                    .idu2exu_pr_byte    (idu2exu_pr_byte    ),
                    .idu2exu_pr_half    (idu2exu_pr_half    ),
                    .idu2exu_pr_unsigned(idu2exu_pr_unsigned),
                    .idu2exu_pr_imme    (idu2exu_pr_imme    ),
                    .idu2exu_pr_sign    (idu2exu_pr_sign    ),
                    .idu2exu_pr_rs1_id  (idu2exu_pr_rs1_id  ),
                    .idu2exu_pr_rs2_id  (idu2exu_pr_rs2_id  ),
                    .idu2exu_pr_rs1     (idu2exu_pr_rs1     ),
                    .idu2exu_pr_rs2     (idu2exu_pr_rs2     ),
                    .idu2exu_pr_aluop   (idu2exu_pr_aluop   ),
                    .idu2exu_pr_optype  (idu2exu_pr_optype  ),
                    .idu2exu_pr_rd_addr (idu2exu_pr_rd_addr ),
                    .idu2exu_pr_mem_web (idu2exu_pr_mem_web ),
                    .idu2exu_pr_mem_oe  (idu2exu_pr_mem_oe  ),
                    .idu2exu_pr_reg_web (idu2exu_pr_reg_web ),
                    .idu2exu_pr_mem2reg (idu2exu_pr_mem2reg ),

                    .idu2exu_wbu_fwd_reg_sel1 (idu2exu_wbu_fwd_reg_sel1),
                    .idu2exu_wbu_fwd_reg_sel2 (idu2exu_wbu_fwd_reg_sel2),
                    .wbu2exu_fwd_reg_data     (wbu2rf_reg_data         ),

                    .exu2pc_jump        (exu2pc_jump     ),
                    .exu2pc_jump_addr   (exu2pc_jump_addr),

                    .exu2mem_pr_pc      (exu2mem_pr_pc      ),
                    .exu2mem_pr_byte    (exu2mem_pr_byte    ),
                    .exu2mem_pr_half    (exu2mem_pr_half    ),
                    .exu2mem_pr_unsigned(exu2mem_pr_unsigned),
                    .exu2mem_rs1_id     (exu2mem_rs1_id     ),
                    .exu2mem_rs2_id     (exu2mem_rs2_id     ),
                    .exu2mem_pr_mem_oe  (exu2mem_pr_mem_oe  ),
                    .exu2mem_pr_mem_web (exu2mem_pr_mem_web ),
                    .exu2mem_pr_mem_addr(exu2mem_pr_mem_addr),
                    .exu2mem_pr_mem_data(exu2mem_pr_mem_data),
                    .exu2mem_pr_reg_web (exu2mem_pr_reg_web ),
                    .exu2mem_pr_reg_addr(exu2mem_pr_reg_addr),
                    .exu2mem_pr_reg_data(exu2mem_pr_reg_data),
                    .exu2mem_pr_mem2reg (exu2mem_pr_mem2reg ),

                    .cpu2dmc_type       (cpu2dmc_type),

                    .mem2exu_fwd_reg_sel1(mem2exu_fwd_reg_sel1),
                    .mem2exu_fwd_reg_sel2(mem2exu_fwd_reg_sel2),
                    .mem2exu_fwd_reg_data(mem2exu_fwd_reg_data)
                   );

  oska_mem OSKA_MEM(.clk(clk),
                    .rst(rst),

                    .wrp2cpu_stall      (wrp2cpu_stall      ),

                    .exu2mem_pr_pc      (exu2mem_pr_pc      ),
                    .exu2mem_pr_byte    (exu2mem_pr_byte    ),
                    .exu2mem_pr_half    (exu2mem_pr_half    ),
                    .exu2mem_pr_unsigned(exu2mem_pr_unsigned),
                    .exu2mem_rs1_id     (exu2mem_rs1_id     ),
                    .exu2mem_rs2_id     (exu2mem_rs2_id     ),
                    .exu2mem_pr_reg_web (exu2mem_pr_reg_web ),
                    .exu2mem_pr_reg_addr(exu2mem_pr_reg_addr),
                    .exu2mem_pr_reg_data(exu2mem_pr_reg_data),
                    .exu2mem_pr_mem2reg (exu2mem_pr_mem2reg ),

                    .mem2exu_fwd_reg_sel1(mem2exu_fwd_reg_sel1),
                    .mem2exu_fwd_reg_sel2(mem2exu_fwd_reg_sel2),
                    .mem2exu_fwd_reg_data(mem2exu_fwd_reg_data),

                    .mem2idu_fwd_reg_web (mem2idu_fwd_reg_web ),
                    .mem2idu_fwd_reg_addr(mem2idu_fwd_reg_addr),
                    .mem2wbu_pr_reg_web  (wbu2rf_reg_web      ),
                    .mem2wbu_pr_reg_addr (wbu2rf_reg_addr     ),

                    .mem2wbu_pr_byte    (mem2wbu_pr_byte    ),
                    .mem2wbu_pr_half    (mem2wbu_pr_half    ),
                    .mem2wbu_pr_unsigned(mem2wbu_pr_unsigned),
                    .mem2wbu_pr_pc      (mem2wbu_pr_pc      ),
                    .mem2wbu_pr_reg_data(mem2wbu_pr_reg_data),
                    .mem2wbu_pr_mem2reg (mem2wbu_pr_mem2reg )
                   );

  oska_wbu OSKA_WBU(.mem2wbu_pr_reg_data(mem2wbu_pr_reg_data),
                    .mem2wbu_pr_pc      (mem2wbu_pr_pc      ),
                    .mem2wbu_pr_byte    (mem2wbu_pr_byte    ),
                    .mem2wbu_pr_half    (mem2wbu_pr_half    ),
                    .mem2wbu_pr_unsigned(mem2wbu_pr_unsigned),
                    .mem2wbu_pr_mem2reg (mem2wbu_pr_mem2reg ),
                    .mem2wbu_mem_data   (dm12cpu_do         ),

                    .wbu2rf_reg_data    (wbu2rf_reg_data    )
                   );

endmodule
