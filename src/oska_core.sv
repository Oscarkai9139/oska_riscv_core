`include "oska_define.sv"
`include "oska_cpu.sv"
`include "def.svh"


module oska_core(input logic clk,
                 input logic rst,

                 // Instruction Cache port
                 input logic                  imc_wait,
                 input logic [`WORD_SIZE-1:0] imc_out,

                 output logic                        imc_req,
                 output logic                        imc_write,
                 output logic [`CACHE_TYPE_BITS-1:0] imc_type,
                 output logic [`WORD_SIZE-1:0]       imc_addr,
                 output logic [`WORD_SIZE-1:0]       imc_in,

                 // Data Cache port
                 input logic                  dmc_wait,
                 input logic [`WORD_SIZE-1:0] dmc_out,

                 output logic                        dmc_req,
                 output logic                        dmc_write,
                 output logic [`CACHE_TYPE_BITS-1:0] dmc_type,
                 output logic [`WORD_SIZE-1:0]       dmc_addr,
                 output logic [`WORD_SIZE-1:0]       dmc_in,
                );

// Stall cpu signal
logic dmc2cpu_stall; // Stall for data memory cache
logic imc2cpu_stall; // Stall for instruction memory cache
logic cpu_stall;

assign cpu_stall = imc2cpu_stall | dmc2cpu_stall;

// Memory cs & oe
logic                  im1_cs;
logic                  im1_oe;
logic [`WORD_SIZE-1:0] im1_data;
logic [3:0]            im1_web;
logic [`WORD_SIZE-1:0] im1_waddr;
logic                  dm1_cs;
logic                  dm1_oe;
logic [3:0]            dm1_web;
logic [`WORD_SIZE-1:0] dm1_addr;
logic [`WORD_SIZE-1:0] dm1_wdata;

logic [`PC_SIZE-1:0]         wrp2cpu_pc;
logic                        wrp2cpu_rready;
logic [`CACHE_TYPE_BITS-1:0] cpu2dmc_type;

logic [`WORD_SIZE-1:0] wrp2cpu_instr_hold;
logic [`WORD_SIZE-1:0] wrp2cpu_rdata_hold;

oska_cpu OSKA_CPU(.clk(clk),
                  .rst(rst),

                  // CPU wrapper to internal cpu
                  .wrp2cpu_stall  (cpu_stall    ),
                  .wrp2cpu_pc     (wrp2cpu_pc   ),
                  .wrp2cpu_rready (wrp2cpu_ready),

                  // Data memory type signal
                  .cpu2dmc_type(cpu2dmc_type),

                  // Data memory port
                  .cpu2dm1_cs  (dm1_cs   ),
                  .cpu2dm1_oe  (dm1_oe   ),
                  .cpu2dm1_web (dm1_web  ),
                  .cpu2dm1_addr(dm1_addr ),
                  .cpu2dm1_di  (dm1_wdata ),
                  .dm12cpu_do  (dmc_out),

                  // Instruction memory
                  .ifu2wrp_cs  (im1_cs   ),
                  .ifu2wrp_oe  (im1_oe   ),
                  .ifu2wrp_web (im1_web  ),
                  .cpu2im1_addr(im1_addr ),
                  .cpu2im1_di  (im1_wdata ),
                  .im12cpu_do  (imc_out)
                  );

// Holds pc for ifu
logic [`WORD_SIZE-1:0] wrp2cpu_pc;

// Finite state machine
logic [1:0] imc_state_cur;
logic [1:0] imc_state_cur;
logic [1:0] dmc_state_next;
logic [1:0] dmc_state_next;

// Easy reading assignment
logic  dmc_read_en;
logic  dmc_write_en;
assign dmc_read_en  = dm1_oe;
assign dmc_write_en = (dm1_web == 4'b1111)? 1'b0: 1'b1;

logic  imc_read_en;
assign imc_read_en = im1_oe;

// Instruction Cache finite state machine
always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    imc_state_cur <= `IMC_IDLE;
  end
  else begin
    imc_state_cur <= imc_state_next;
  end
end


// State change for IMC
always_comb begin
  case(imc_state_cur)
    `IMC_IDLE:imc_state_next = (imc_read_en)? `IMC_REQ: `IMC_IDLE;
    `IMC_REQ :imc_state_next = `IMC_MISS;
    `IMC_MISS:imc_state_next = (~imc_wait & dmc2cpu_stall)? `IMC_WAIT:
                               (~imc_wait & ~dmc2cpu_stall)? 
                               `IMC_IDLE: `IMC_MISS;
    `IMC_WAIT:imc_state_next = (dmc_state_cur == `DMC_IDLE |
                                dmc_state_cur == `DMC_WAIT)?
                                `IMC_IDLE: `IMC_WAIT;
  endcase
end

always_ff @(posedge clk, posedge rst) begin
  if(rst) begin
    wrp2cpu_pc <= `WORD_SIZE'b0
  end
  else begin
    case (imc_state_cur)
      `IMC_IDLE: wrp2cpu_pc <= (imc_read_en)? im1_addr: `IM_ADDR_SIZE'b0;
      `IMC_REQ:  wrp2cpu_pc <= wrp2cpu_pc;
      `IMC_MISS: wrp2cpu_pc <= wrp2cpu_pc;
      `IMC_WAIT: wrp2cpu_pc <= wrp2cpu_pc;
    endcase
  end
end

// IMC state
always_comb begin
  case(imc_state_cur)
    `IMC_IDLE: begin
      imc2cpu_stall = 1'b0;
      imc_req       = 1'b0;
      imc_addr      = 32'b0;
      imc_write     = 1'b0;
      imc_in        = 32'b0;
      wrp2cpu_ready = 1'b1;
    end
    `IMC_REQ: begin
      imc2cpu_stall = 1'b1;
      imc_req       = 1'b1;
      imc_addr      = im1_addr;
      imc_write     = 1'b0;
      imc_in        = 32'b0;
      imc_type      = `CACHE_WORD;
      wrp2cpu_ready = 1'b0;
    end
    `IMC_MISS: begin
      imc2cpu_stall = 1'b1;
      imc_req       = 1'b0;
      imc_addr      = 32'b0;
      imc_write     = 1'b0;
      imc_in        = 32'b0;
      wrp2cpu_ready = 1'b0;
    end
    `IMC_WAIT: begin
      imc2cpu_stall = 1'b1;
      imc_req       = 1'b0;
      imc_addr      = 32'b0;
      imc_write     = 1'b0;
      imc_in        = 32'b0;
      wrp2cpu_ready = 1'b0;
    end
  endcase
end

// Data MEM Cache finite state machine
always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    dmc_state_cur <= `DMC_IDLE;
  end
  else begin
    dmc_state_cur <= dmc_state_next;
  end
end

// State change for IMC
always_comb begin
  case(dmc_state_cur)
    `DMC_IDLE:dmc_state_next = (dmc_read_en | dmc_write_en)? `DMC_REQ: `DMC_IDLE;
    `DMC_REQ :dmc_state_next = `DMC_MISS;
    `DMC_MISS:dmc_state_next = (~dmc_wait & imc2cpu_stall)? `DMC_WAIT:
                               (~dmc_wait & ~i mc2cpu_stall)? 
                               `DMC_IDLE: `DMC_MISS;
    `DMC_WAIT:dmc_state_next = (imc_state_cur == `IMC_IDLE |
                                imc_state_cur == `IMC_WAIT)?
                                `DMC_IDLE: `DMC_WAIT;
  endcase
end

// DMC state
always_comb begin
  case(dmc_state_cur)
    `IMC_IDLE: begin
      dmc2cpu_stall = 1'b0;
      dmc_req       = 1'b0;
      dmc_addr      = 32'b0;
      dmc_write     = 1'b0;
      dmc_in        = 32'b0;
    end
    `IMC_REQ: begin
      dmc2cpu_stall = 1'b1;
      dmc_req       = 1'b1;
      dmc_addr      = dm1_addr;
      dmc_write     = ~dm1_oe;
      dmc_in        = dm1_wdata;
      dmc_type      = cpu2dmc_type;
    end
    `IMC_MISS: begin
      dmc2cpu_stall = 1'b1;
      dmc_req       = 1'b0;
      dmc_addr      = 32'b0;
      dmc_write     = 1'b0;
      dmc_in        = 32'b0;
    end
    `IMC_WAIT: begin
      dmc2cpu_stall = 1'b1;
      dmc_req       = 1'b0;
      dmc_addr      = 32'b0;
      dmc_write     = 1'b0;
      dmc_in        = 32'b0;
    end
  endcase
end


endmodule
