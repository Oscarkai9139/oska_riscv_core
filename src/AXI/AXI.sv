//================================================
// Auther:      Chang Wan-Yun (Claire)            
// Filename:    AXI.sv                            
// Description: Top module of AXI                  
// Version:     1.0 
//================================================
`include "AXI_define.svh"

module AXI(

	input ACLK,
	input ARESETn,

	//SLAVE INTERFACE FOR MASTERS
	//WRITE ADDRESS
	input [`AXI_ID_BITS-1:0] AWID_M1,
	input [`AXI_ADDR_BITS-1:0] AWADDR_M1,
	input [`AXI_LEN_BITS-1:0] AWLEN_M1,
	input [`AXI_SIZE_BITS-1:0] AWSIZE_M1,
	input [1:0] AWBURST_M1,
	input AWVALID_M1,
	output reg AWREADY_M1,
	//WRITE DATA
	input [`AXI_DATA_BITS-1:0] WDATA_M1,
	input [`AXI_STRB_BITS-1:0] WSTRB_M1,
	input WLAST_M1,
	input WVALID_M1,
	output reg WREADY_M1,
	//WRITE RESPONSE
	output reg [`AXI_ID_BITS-1:0] BID_M1,
	output reg [1:0] BRESP_M1,
	output reg BVALID_M1,
	input BREADY_M1,

	//READ ADDRESS0
	input [`AXI_ID_BITS-1:0] ARID_M0,
	input [`AXI_ADDR_BITS-1:0] ARADDR_M0,
	input [`AXI_LEN_BITS-1:0] ARLEN_M0,
	input [`AXI_SIZE_BITS-1:0] ARSIZE_M0,
	input [1:0] ARBURST_M0,
	input ARVALID_M0,
	output reg ARREADY_M0,
	//READ DATA0
	output reg [`AXI_ID_BITS-1:0] RID_M0,
	output reg [`AXI_DATA_BITS-1:0] RDATA_M0,
	output reg [1:0] RRESP_M0,
	output reg RLAST_M0,
	output reg RVALID_M0,
	input RREADY_M0,
	//READ ADDRESS1
	input [`AXI_ID_BITS-1:0] ARID_M1,
	input [`AXI_ADDR_BITS-1:0] ARADDR_M1,
	input [`AXI_LEN_BITS-1:0] ARLEN_M1,
	input [`AXI_SIZE_BITS-1:0] ARSIZE_M1,
	input [1:0] ARBURST_M1,
	input ARVALID_M1,
	output reg ARREADY_M1,
	//READ DATA1
	output reg [`AXI_ID_BITS-1:0] RID_M1,
	output reg [`AXI_DATA_BITS-1:0] RDATA_M1,
	output reg [1:0] RRESP_M1,
	output reg RLAST_M1,
	output reg RVALID_M1,
	input RREADY_M1,

	//MASTER INTERFACE FOR SLAVES
	//WRITE ADDRESS0
	output reg [`AXI_IDS_BITS-1:0] AWID_S0,
	output reg [`AXI_ADDR_BITS-1:0] AWADDR_S0,
	output reg [`AXI_LEN_BITS-1:0] AWLEN_S0,
	output reg [`AXI_SIZE_BITS-1:0] AWSIZE_S0,
	output reg [1:0] AWBURST_S0,
	output reg AWVALID_S0,
	input AWREADY_S0,
	//WRITE DATA0
	output reg [`AXI_DATA_BITS-1:0] WDATA_S0,
	output reg [`AXI_STRB_BITS-1:0] WSTRB_S0,
	output reg WLAST_S0,
	output reg WVALID_S0,
	input WREADY_S0,
	//WRITE RESPONSE0
	input [`AXI_IDS_BITS-1:0] BID_S0,
	input [1:0] BRESP_S0,
	input BVALID_S0,
	output reg BREADY_S0,
	
	//WRITE ADDRESS1
	output reg [`AXI_IDS_BITS-1:0] AWID_S1,
	output reg [`AXI_ADDR_BITS-1:0] AWADDR_S1,
	output reg [`AXI_LEN_BITS-1:0] AWLEN_S1,
	output reg [`AXI_SIZE_BITS-1:0] AWSIZE_S1,
	output reg [1:0] AWBURST_S1,
	output reg AWVALID_S1,
	input AWREADY_S1,
	//WRITE DATA1
	output reg [`AXI_DATA_BITS-1:0] WDATA_S1,
	output reg [`AXI_STRB_BITS-1:0] WSTRB_S1,
	output reg WLAST_S1,
	output reg WVALID_S1,
	input WREADY_S1,
	//WRITE RESPONSE1
	input [`AXI_IDS_BITS-1:0] BID_S1,
	input [1:0] BRESP_S1,
	input BVALID_S1,
	output reg BREADY_S1,
	
	//READ ADDRESS0
	output reg [`AXI_IDS_BITS-1:0] ARID_S0,
	output reg [`AXI_ADDR_BITS-1:0] ARADDR_S0,
	output reg [`AXI_LEN_BITS-1:0] ARLEN_S0,
	output reg [`AXI_SIZE_BITS-1:0] ARSIZE_S0,
	output reg [1:0] ARBURST_S0,
	output reg ARVALID_S0,
	input ARREADY_S0,
	//READ DATA0
	input [`AXI_IDS_BITS-1:0] RID_S0,
	input [`AXI_DATA_BITS-1:0] RDATA_S0,
	input [1:0] RRESP_S0,
	input RLAST_S0,
	input RVALID_S0,
	output reg RREADY_S0,
	//READ ADDRESS1
	output reg [`AXI_IDS_BITS-1:0] ARID_S1,
	output reg [`AXI_ADDR_BITS-1:0] ARADDR_S1,
	output reg [`AXI_LEN_BITS-1:0] ARLEN_S1,
	output reg [`AXI_SIZE_BITS-1:0] ARSIZE_S1,
	output reg [1:0] ARBURST_S1,
	output reg ARVALID_S1,
	input ARREADY_S1,
	//READ DATA1
	input [`AXI_IDS_BITS-1:0] RID_S1,
	input [`AXI_DATA_BITS-1:0] RDATA_S1,
	input [1:0] RRESP_S1,
	input RLAST_S1,
	input RVALID_S1,
	output reg RREADY_S1
	
);
    //---------- you should put your design here ----------//
	localparam DEFAULT_M = 2'b00;
	localparam M0        = 2'b01;
	localparam M1        = 2'b10;
	localparam M0_ID     = 4'b0000;
	localparam M1_ID     = 4'b0001;
	localparam DEFAULT_S = 2'b00;
	localparam S0        = 2'b01;
	localparam S1        = 2'b10;
	localparam S0_ADDR_MASK = 16'h0000;
	localparam S1_ADDR_MASK = 16'h0001;

	//Read channel register
	reg                       R_hold; //Hold connection when high
	reg [1:0]                 R_master;
	reg [1:0]                 R_slave;
	reg [`AXI_IDS_BITS-1:0]   arid;
	reg [`AXI_ADDR_BITS-1:0]  araddr;
	reg [`AXI_LEN_BITS-1:0]   arlen;
	reg [`AXI_SIZE_BITS-1:0]  arsize;
	reg [`AXI_BURST_BITS-1:0] arburst;
	reg                       arvalid;
	reg                       arready;
	reg [`AXI_IDS_BITS-1:0]   rid;
	reg [`AXI_DATA_BITS-1:0]  rdata;
	reg [`AXI_RESP_BITS-1:0]  rresp;
	reg                       rlast;
	reg                       rvalid;
	reg                       rready;

	//Write channel register
	reg                       W_hold; //Hold connection when high
	reg [1:0]                 W_master;
	reg [1:0]                 W_slave;
	reg [`AXI_IDS_BITS-1:0]   awid;
	reg [`AXI_ADDR_BITS-1:0]  awaddr;
	reg [`AXI_LEN_BITS-1:0]   awlen;
	reg [`AXI_SIZE_BITS-1:0]  awsize;
	reg [`AXI_BURST_BITS-1:0] awburst;
	reg                       awvalid;
	reg                       awready;
	reg [`AXI_DATA_BITS-1:0]  wdata;
	reg [`AXI_STRB_BITS-1:0]  wstrb;
	reg                       wlast;
	reg                       wvalid;
	reg                       wready;
	reg [`AXI_IDS_BITS-1:0]   bid;
	reg [`AXI_RESP_BITS-1:0]  bresp;
	reg                       bvalid;
	reg                       bready;

	//Fake module of default slave
	reg                       default_awready;
	reg                       default_wready;
	reg [`AXI_IDS_BITS-1:0]   default_bid;
	reg [`AXI_RESP_BITS-1:0]  default_bresp;
	reg                       default_bvalid;
	reg                       default_arready;
	reg [`AXI_IDS_BITS-1:0]   default_rid;
	reg [`AXI_DATA_BITS-1:0]  default_rdata;
	reg [`AXI_RESP_BITS-1:0]  default_rresp;
	reg                       default_rlast;
	reg                       default_rvalid;

	//Decide which master/slave can issue request/response
	//Read channel
	//Arbitor
	always_ff @(posedge ACLK) begin
		unique if(ARESETn == 1'b0) begin
			R_hold   = 1'b0;
			R_master = DEFAULT_M;
			R_slave  = DEFAULT_S;
		end else begin
			unique if (R_hold) begin
				if (rready && rvalid) begin
					//Transaction is over
					R_hold   = 1'b0;
				end else begin
					//Connection keeps holding on
				end
			end else begin
				//Setup next transaction
				if (ARVALID_M1) begin
					R_hold   = 1'b1;
					R_master = M1;
					R_slave  = (ARADDR_M1[31:16] == S0_ADDR_MASK)?S0:(ARADDR_M1[31:16] == S1_ADDR_MASK)?S1:DEFAULT_S;
				end else if (ARVALID_M0) begin
					R_hold   = 1'b1;
					R_master = M0;
					R_slave  = (ARADDR_M0[31:16] == S0_ADDR_MASK)?S0:(ARADDR_M0[31:16] == S1_ADDR_MASK)?S1:DEFAULT_S;
				end else begin
					//No any master issues request
				end
			end
		end
	end

	//Request parameter
	always_comb begin
		unique if (R_hold) begin
			unique case (R_master)
				M0: begin
					arid     = {ARID_M0, M0_ID};
					araddr   = ARADDR_M0;
					arlen    = ARLEN_M0;
					arsize   = ARSIZE_M0;
					arburst  = ARBURST_M0;
					arvalid  = ARVALID_M0;
					rready   = RREADY_M0;
				end
				M1: begin
					arid     = {ARID_M1, M1_ID};
					araddr   = ARADDR_M1;
					arlen    = ARLEN_M1;
					arsize   = ARSIZE_M1;
					arburst  = ARBURST_M1;
					arvalid  = ARVALID_M1;
					rready   = RREADY_M1;
				end
				default: begin
					arid     = {`AXI_IDS_BITS{1'b0}};
					araddr   = {`AXI_ADDR_BITS{1'b0}};
					arlen    = {`AXI_LEN_BITS{1'b0}};
					arsize   = {`AXI_SIZE_BITS{1'b0}};
					arburst  = {`AXI_BURST_BITS{1'b0}};
					arvalid  = 1'b0;
					rready   = 1'b0;
				end
			endcase
		end else begin
			//No transaction
			arid     = {`AXI_IDS_BITS{1'b0}};
			araddr   = {`AXI_ADDR_BITS{1'b0}};
			arlen    = {`AXI_LEN_BITS{1'b0}};
			arsize   = {`AXI_SIZE_BITS{1'b0}};
			arburst  = {`AXI_BURST_BITS{1'b0}};
			arvalid  = 1'b0;
		rready   = 1'b0;
		end
	end

	//Response parameter
	always_comb begin
		unique if (R_hold) begin
			unique case (R_slave)
				DEFAULT_S: begin
					arready = default_arready;
					rid     = default_rid;
					rdata   = default_rdata;
					rresp   = default_rresp;
					rlast   = default_rlast;
					rvalid  = default_rvalid;
				end
				S0: begin
					arready = ARREADY_S0;
					rid     = RID_S0;
					rdata   = RDATA_S0;
					rresp   = RRESP_S0;
					rlast   = RLAST_S0;
					rvalid  = RVALID_S0;
				end
				S1: begin
					arready = ARREADY_S1;
					rid     = RID_S1;
					rdata   = RDATA_S1;
					rresp   = RRESP_S1;
					rlast   = RLAST_S1;
					rvalid  = RVALID_S1;
				end
				default: begin
					arready = 1'b0;
					rid     = {`AXI_IDS_BITS{1'b0}};
					rdata   = {`AXI_DATA_BITS{1'b0}};
					rresp   = {`AXI_RESP_BITS{1'b0}};
					rlast   = 1'b0;
					rvalid  = 1'b0;
				end
			endcase
		end else begin
			//No transaction
			arready = 1'b0;
			rid     = {`AXI_IDS_BITS{1'b0}};
			rdata   = {`AXI_DATA_BITS{1'b0}};
			rresp   = {`AXI_RESP_BITS{1'b0}};
			rlast   = 1'b0;
			rvalid  = 1'b0;
		end
	end

	//Write channel
	//Arbitor
	always_ff @(posedge ACLK) begin
		unique if (ARESETn == 1'b0) begin
			W_hold   = 1'b0;
			W_master = DEFAULT_M;
			W_slave  = DEFAULT_S;
		end else begin
			unique if (W_hold) begin
				if (bready && bvalid) begin
					//Transaction is over
					W_hold   = 1'b0;
				end else begin
					//Connection keeps holding on
				end
			end else begin
				//Setup next transaction
				if (AWVALID_M1) begin
					W_hold   = 1'b1;
					W_master = M1;
					W_slave  = (AWADDR_M1[31:16] & S0_ADDR_MASK)?S0:(AWADDR_M1[31:16] & S1_ADDR_MASK)?S1:DEFAULT_S;
				end else begin
					//No any master issues request
				end
			end
		end
	end

	//Request parameter
	always_comb begin
		unique if (W_hold) begin
			unique case (W_master)
				M1: begin
					awid     = {AWID_M1, M1_ID};
					awaddr   = AWADDR_M1;
					awlen    = AWLEN_M1;
					awsize   = AWSIZE_M1;
					awburst  = AWBURST_M1;
					awvalid  = AWVALID_M1;
					wdata    = WDATA_M1;
					wstrb    = WSTRB_M1;
					wlast    = WLAST_M1;
					wvalid   = WVALID_M1;
					bready   = BREADY_M1;
				end
				default: begin
					awid     = {`AXI_IDS_BITS{1'b0}};
					awaddr   = {`AXI_ADDR_BITS{1'b0}};
					awlen    = {`AXI_LEN_BITS{1'b0}};
					awsize   = {`AXI_SIZE_BITS{1'b0}};
					awburst  = {`AXI_BURST_BITS{1'b0}};
					awvalid  = 1'b0;
					wdata    = {`AXI_DATA_BITS{1'b0}};
					wstrb    = {`AXI_STRB_BITS{1'b0}};
					wlast    = 1'b0;
					wvalid   = 1'b0;
					bready   = 1'b0;
				end
			endcase
		end else begin
			//No transaction
			awid     = {`AXI_IDS_BITS{1'b0}};
			awaddr   = {`AXI_ADDR_BITS{1'b0}};
			awlen    = {`AXI_LEN_BITS{1'b0}};
			awsize   = {`AXI_SIZE_BITS{1'b0}};
			awburst  = {`AXI_BURST_BITS{1'b0}};
			awvalid  = 1'b0;
			wdata    = {`AXI_DATA_BITS{1'b0}};
			wstrb    = {`AXI_STRB_BITS{1'b0}};
			wlast    = 1'b0;
			wvalid   = 1'b0;
			bready   = 1'b0;
		end
	end

	//Response parameter
	always_comb begin
		unique if (W_hold) begin
			unique case (W_slave)
				DEFAULT_S: begin
					awready = default_awready;
					wready  = default_wready;
					bid     = default_bid;
					bresp   = default_bresp;
					bvalid  = default_bvalid;
				end
				S0: begin
					awready = AWREADY_S0;
					wready  = WREADY_S0;
					bid     = BID_S0;
					bresp   = BRESP_S0;
					bvalid  = BVALID_S0;
				end
				S1: begin
					awready = AWREADY_S1;
					wready  = WREADY_S1;
					bid     = BID_S1;
					bresp   = BRESP_S1;
					bvalid  = BVALID_S1;
				end
				default: begin
					awready = 1'b0;
					wready  = 1'b0;
					bid     = {`AXI_IDS_BITS{1'b0}};
					bresp   = {`AXI_RESP_BITS{1'b0}};
					bvalid  = 1'b0;
				end
			endcase
		end else begin
			//No transaction
			awready = 1'b0;
			wready  = 1'b0;
			bid     = {`AXI_IDS_BITS{1'b0}};
			bresp   = {`AXI_RESP_BITS{1'b0}};
			bvalid  = 1'b0;
		end
	end
	
	//M0
	//READ channel
	always_comb begin
		unique if (R_hold && (R_master==M0)) begin
			ARREADY_M0 = arready;
			RID_M0     = rid[7:4];
			RDATA_M0   = rdata;
			RRESP_M0   = rresp;
			RVALID_M0  = rvalid;
			RLAST_M0   = rlast;
		end else begin
			ARREADY_M0 = 1'b0;
			RID_M0     = {`AXI_ID_BITS{1'b0}};
			RDATA_M0   = {`AXI_DATA_BITS{1'b0}};
			RRESP_M0   = `AXI_RESP_OKAY;
			RVALID_M0  = 1'b0;
			RLAST_M0   = 1'b0;
		end
	end

	//M1
	//Read channel
	always_comb begin
		unique if (R_hold && (R_master==M1)) begin
			ARREADY_M1 = arready;
			RID_M1     = rid[7:4];
			RDATA_M1   = rdata;
			RRESP_M1   = rresp;
			RVALID_M1  = rvalid;
			RLAST_M1   = rlast;
		end else begin
			ARREADY_M1 = 1'b0;
			RID_M1     = {`AXI_ID_BITS{1'b0}};
			RDATA_M1   = {`AXI_DATA_BITS{1'b0}};
			RRESP_M1   = `AXI_RESP_OKAY;
			RVALID_M1  = 1'b0;
			RLAST_M1   = 1'b0;
		end
	end
	//Write channel
	always_comb begin
		unique if (W_hold && (W_master==M1)) begin
			AWREADY_M1 = awready;
			WREADY_M1  = wready;
			BID_M1     = bid[7:4];
			BRESP_M1   = bresp;
			BVALID_M1  = bvalid;
		end else begin
			AWREADY_M1 = 1'b0;
			WREADY_M1  = 1'b0;
			BID_M1     = {`AXI_ID_BITS{1'b0}};
			BRESP_M1   = `AXI_RESP_OKAY;
			BVALID_M1  = 1'b0;
		end
	end

	//Master interfaces for slave module
	//Default_S
	//Read Channel
	assign default_rdata = {`AXI_DATA_BITS{1'b0}};
	assign default_rresp = `AXI_RESP_DECERR;
	assign default_rlast = 1'b1;
	always_ff @(posedge ACLK or negedge ARESETn) begin
		unique if(ARESETn) begin
			unique if (R_hold && (R_slave==DEFAULT_S)) begin
				if (default_arready && arvalid) begin
					default_arready <= 1'b0;
					default_rvalid  <= 1'b1;
					default_rid     <= arid;
				end else if (default_rvalid && rready) begin
					default_arready <= 1'b1;
					default_rvalid  <= 1'b0;
					default_rid     <= default_rid;
				end else begin
					default_arready <= default_arready;
					default_rvalid  <= default_rvalid;
					default_rid     <= default_rid;
				end
			end else begin
				default_arready <= default_arready;
				default_rvalid  <= default_rvalid;
				default_rid     <= default_rid;
			end
		end else begin
			//Reset
			default_arready <= 1'b1;
			default_rvalid  <= 1'b0;
			default_rid     <= {`AXI_IDS_BITS{1'b0}};
		end
	end
	//Write Channel
	assign default_bresp   = `AXI_RESP_DECERR;
	always_ff @(posedge ACLK or negedge ARESETn) begin
		unique if(ARESETn) begin
			unique if (W_hold && (W_slave==DEFAULT_S)) begin
				if (default_awready && awvalid) begin
					default_awready <= 1'b0;
					default_wready  <= 1'b1;
					default_bvalid  <= 1'b0;
					default_bid     <= awid;
				end else if (default_wready && wvalid) begin
					default_awready <= 1'b0;
					default_wready  <= 1'b0;
					default_bvalid  <= 1'b1;
					default_bid     <= default_bid;
				end else if (default_bvalid && bready) begin
					default_awready <= 1'b1;
					default_wready  <= 1'b0;
					default_bvalid  <= 1'b0;
					default_bid     <= default_bid;
				end else begin
					default_awready <= default_awready;
					default_wready  <= default_wready;
					default_bvalid  <= default_bvalid;
					default_bid     <= default_bid;
				end
			end else begin
				default_awready <= default_awready;
				default_wready  <= default_wready;
				default_bvalid  <= default_bvalid;
				default_bid     <= default_bid;
			end
		end else begin
			//Reset
			default_awready <= 1'b1;
			default_wready  <= 1'b0;
			default_bid     <= {`AXI_IDS_BITS{1'b0}};
			default_bvalid  <= 1'b0;
		end
	end

	//S0
	//Read channel
	always_comb begin
		unique if (R_hold && (R_slave==S0)) begin
			ARID_S0    = arid;
			ARADDR_S0  = araddr;
			ARLEN_S0   = arlen;
			ARSIZE_S0  = arsize;
			ARBURST_S0 = arburst;
			ARVALID_S0 = arvalid;
			RREADY_S0  = rready;
		end else begin
			ARID_S0    = {`AXI_IDS_BITS{1'b0}};
			ARADDR_S0  = {`AXI_ADDR_BITS{1'b0}};
			ARLEN_S0   = {`AXI_LEN_BITS{1'b0}};
			ARSIZE_S0  = {`AXI_SIZE_BITS{1'b0}};
			ARBURST_S0 = {`AXI_BURST_BITS{1'b0}};
			ARVALID_S0 = 1'b0;
			RREADY_S0  = 1'b0;
		end
	end
	//Write channel
	always_comb begin
		unique if (W_hold && (W_slave==S0)) begin
			AWID_S0    = awid;
			AWADDR_S0  = awaddr;
			AWLEN_S0   = awlen;
			AWSIZE_S0  = awsize;
			AWBURST_S0 = awburst;
			AWVALID_S0 = awvalid;
			WDATA_S0   = wdata;
			WSTRB_S0   = wstrb;
			WVALID_S0  = wvalid;
			WLAST_S0   = wlast;
			BREADY_S0  = bready;
		end else begin
			AWID_S0    = {`AXI_IDS_BITS{1'b0}};
			AWADDR_S0  = {`AXI_ADDR_BITS{1'b0}};
			AWLEN_S0   = {`AXI_LEN_BITS{1'b0}};
			AWSIZE_S0  = {`AXI_SIZE_BITS{1'b0}};
			AWBURST_S0 = {`AXI_BURST_BITS{1'b0}};
			AWVALID_S0 = 1'b0;
			WDATA_S0   = {`AXI_DATA_BITS{1'b0}};
			WSTRB_S0   = {`AXI_STRB_BITS{1'b0}};
			WVALID_S0  = 1'b0;
			WLAST_S0   = 1'b0;
			BREADY_S0  = 1'b0;
		end
	end

	//S1
	//Read channel
	always_comb begin
		unique if (R_hold && (R_slave==S1)) begin
			ARID_S1    = arid;
			ARADDR_S1  = araddr;
			ARLEN_S1   = arlen;
			ARSIZE_S1  = arsize;
			ARBURST_S1 = arburst;
			ARVALID_S1 = arvalid;
			RREADY_S1  = rready;
		end else begin
			ARID_S1    = {`AXI_IDS_BITS{1'b0}};
			ARADDR_S1  = {`AXI_ADDR_BITS{1'b0}};
			ARLEN_S1   = {`AXI_LEN_BITS{1'b0}};
			ARSIZE_S1  = {`AXI_SIZE_BITS{1'b0}};
			ARBURST_S1 = {`AXI_BURST_BITS{1'b0}};
			ARVALID_S1 = 1'b0;
			RREADY_S1  = 1'b0;
		end
	end
	//Write channel
	always_comb begin
		unique if (W_hold && (W_slave==S1)) begin
			AWID_S1    = awid;
			AWADDR_S1  = awaddr;
			AWLEN_S1   = awlen;
			AWSIZE_S1  = awsize;
			AWBURST_S1 = awburst;
			AWVALID_S1 = awvalid;
			WDATA_S1   = wdata;
			WSTRB_S1   = wstrb;
			WVALID_S1  = wvalid;
			WLAST_S1   = wlast;
			BREADY_S1  = bready;
		end else begin
			AWID_S1    = {`AXI_IDS_BITS{1'b0}};
			AWADDR_S1  = {`AXI_ADDR_BITS{1'b0}};
			AWLEN_S1   = {`AXI_LEN_BITS{1'b0}};
			AWSIZE_S1  = {`AXI_SIZE_BITS{1'b0}};
			AWBURST_S1 = {`AXI_BURST_BITS{1'b0}};
			AWVALID_S1 = 1'b0;
			WDATA_S1   = {`AXI_DATA_BITS{1'b0}};
			WSTRB_S1   = {`AXI_STRB_BITS{1'b0}};
			WVALID_S1  = 1'b0;
			WLAST_S1   = 1'b0;
			BREADY_S1  = 1'b0;
		end
	end
endmodule
