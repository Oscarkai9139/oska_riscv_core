`ifndef CACHE_CONTROLLER
`define CACHE_CONTROLLER

`include "def.svh"
module Controller(
  input clk,
  input rst,

  input HIT,
  output logic CA_UPDATE,
  output logic CA_IN_SEL, //0:choose data from CPU, 1:choose data from memory
  output logic [15:0] CA_WEB,

  // Core to CPU wrapper
  input [`DATA_BITS-1:0] C_ADDR,
  input C_REQ,
  input C_WRITE,
  input [`CACHE_TYPE_BITS-1:0] C_TYPE,
  // Mem to CPU wrapper
  input D_WAIT,
  // CPU wrapper to core
  output logic C_WAIT,
  // CPU wrapper to Mem
  output logic D_REQ,
  output logic [`DATA_BITS-1:0] D_ADDR
);

  //Parameter define
  localparam IDLE       = 4'd0;
  localparam READ       = 4'd1;
  localparam READ_MISS  = 4'd2;
  localparam READ_DATA  = 4'd3;
  localparam WRITE      = 4'd5;
  localparam WRITE_HIT  = 4'd6;
  localparam WRITE_SYS  = 4'd7;

  //Register
  logic [1:0] write_length;
  logic [3:0] byte_sel;
  logic       read_finish;
  logic [1:0] read_num;
  logic [3:0] state;
  logic [3:0] next_state;

  //Write enable signal to data array
  assign write_length = (C_TYPE==`CACHE_WORD)?2'b11:(C_TYPE==`CACHE_HWORD || C_TYPE==`CACHE_HWORD_U)?2'b10:(C_TYPE==`CACHE_BYTE || C_TYPE==`CACHE_BYTE_U)?2'b01:2'b00;
  always_comb begin
	  case (write_length)
		  2'b11: begin
			  //Word
			  byte_sel = 4'b0000;
		  end
		  2'b10: begin
			  //Hword
			  case (C_ADDR[1:0])
				  2'b00: begin
					  byte_sel = 4'b1100;
				  end
				  2'b10: begin
					  byte_sel = 4'b0011;
				  end
				  default: begin
					  byte_sel = 4'b1111;
				  end
			  endcase
		  end
		  2'b01: begin
			  //Byte
			  case (C_ADDR[1:0])
				  2'b00: begin
					  byte_sel = 4'b1110;
				  end
				  2'b01: begin
					  byte_sel = 4'b1101;
				  end
				  2'b10: begin
					  byte_sel = 4'b1011;
				  end
				  2'b11: begin
					  byte_sel = 4'b0111;
				  end
				  default: begin
					  byte_sel = 4'b1111;
				  end
			  endcase
		  end
		  default: begin
			  byte_sel = 4'b1111;
		  end
	  endcase
  end
  
  always_comb begin
	  unique if (CA_UPDATE) begin
		  unique if (C_WRITE) begin
			  //Write-hit
			  unique case (C_ADDR[3:2])
				  2'b00: begin
					 CA_WEB = {12'b111111111111, byte_sel}; 
				  end
				  2'b01: begin
					 CA_WEB = {8'b11111111, byte_sel, 4'b1111}; 
				  end
				  2'b10: begin
					 CA_WEB = {4'b1111, byte_sel, 8'b11111111}; 
				  end
				  2'b11: begin
					 CA_WEB = {byte_sel, 12'b111111111111}; 
				  end
			  endcase
		  end else begin
			  //READ-miss
			  unique case (D_ADDR[3:2])
				  2'b00: begin
					 CA_WEB = {12'b111111111111, 4'b0000}; 
				  end
				  2'b01: begin
					 CA_WEB = {8'b11111111, 4'b0000, 4'b1111}; 
				  end
				  2'b10: begin
					 CA_WEB = {4'b1111, 4'b0000, 8'b11111111}; 
				  end
				  2'b11: begin
					 CA_WEB = {4'b0000, 12'b111111111111}; 
				  end
			  endcase
		  end
	  end else begin
		  CA_WEB = 16'b1111111111111111;
	  end
  end

  //Read-miss address controll
  always @(state or next_state) begin
	case (next_state)
		READ_MISS: begin
			if (state==READ) begin
				read_num = 2'b11;
			end else if (state==READ_DATA) begin
				read_num = read_num-1;
			end else begin
				read_num = read_num;
			end
		end
		default: begin
			read_num = read_num;
		end
	endcase
  end
  always_ff @(posedge clk) begin
	  unique if (rst) begin
		  read_finish <= 1'b0;
	  end else begin
		  if (state == READ_DATA && read_num==0) begin
			  read_finish <= 1'b1;
		  end else if (state==IDLE) begin
			  read_finish <= 1'b0;
		  end else begin
			  read_finish <= read_finish;
		  end
	  end
  end

  //State control
  always_ff @(posedge clk)begin
	unique if(rst) begin
		state <= IDLE;
	end else begin
		state <= next_state;
	end
  end
  always_comb begin
	unique case (state)
		IDLE: begin
			unique if (C_REQ) begin
				unique if (C_WRITE) begin
					next_state = WRITE;
					CA_UPDATE  = 1'b0;
					CA_IN_SEL  = 1'b0;
					C_WAIT     = 1'b1;
					D_REQ      = 1'b0;
					D_ADDR     = C_ADDR;
				end else begin
					next_state = READ;
					CA_UPDATE  = 1'b0;
					CA_IN_SEL  = 1'b0;
					C_WAIT     = 1'b1;
					D_REQ      = 1'b0;
					D_ADDR     = C_ADDR;
				end
			end else begin
				next_state = IDLE;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b0;
				D_REQ      = 1'b0;
				D_ADDR     = C_ADDR;
			end
		end
		READ: begin
			unique if (HIT) begin
				next_state = IDLE;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b0;
				D_REQ      = 1'b0;
				D_ADDR     = C_ADDR;
			end else begin
				next_state = READ_MISS;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b1;
				D_ADDR     = {C_ADDR[31:4], read_num, 2'b00};
			end
		end
		READ_MISS: begin
			unique if (D_WAIT) begin
				next_state = READ_MISS;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b1;
				D_ADDR     = {C_ADDR[31:4], read_num, 2'b00};
			end else begin
				next_state = READ_DATA;
				CA_UPDATE  = 1'b1;
				CA_IN_SEL  = 1'b1;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b0;
				D_ADDR     = {C_ADDR[31:4], read_num, 2'b00};
			end
		end
		READ_DATA : begin
			unique if (read_finish) begin
				next_state = IDLE;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b0;
				D_REQ      = 1'b0;
				D_ADDR     = C_ADDR;
			end else begin
				next_state = READ_MISS;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b1;
				D_ADDR     = {C_ADDR[31:4], read_num, 2'b00};
			end
		end
		WRITE: begin
			unique if (HIT) begin
				next_state = WRITE_HIT;
				CA_UPDATE  = 1'b1;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b0;
				D_ADDR     = C_ADDR;
			end else begin
				next_state = WRITE_SYS;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b1;
				D_ADDR     = C_ADDR;
			end
		end
		WRITE_HIT: begin
			next_state = WRITE_SYS;
			CA_UPDATE  = 1'b0;
			CA_IN_SEL  = 1'b0;
			C_WAIT     = 1'b1;
			D_REQ      = 1'b1;
			D_ADDR     = C_ADDR;
		end
		WRITE_SYS: begin
			unique if (D_WAIT) begin
				next_state = WRITE_SYS;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b1;
				D_REQ      = 1'b1;
				D_ADDR     = C_ADDR;
			end else begin
				next_state = IDLE;
				CA_UPDATE  = 1'b0;
				CA_IN_SEL  = 1'b0;
				C_WAIT     = 1'b0;
				D_REQ      = 1'b0;
				D_ADDR     = C_ADDR;
			end
		end
		default: begin
			next_state = next_state;
			CA_UPDATE  = CA_UPDATE;
			CA_IN_SEL  = CA_IN_SEL;
			C_WAIT     = C_WAIT;
			D_REQ      = D_REQ;
			D_ADDR     = D_ADDR;
		end
	endcase
  end
  
endmodule
`endif
