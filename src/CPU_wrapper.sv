`include "oska_define.sv"
`include "../include/AXI_define.svh"
`include "oska_cpu.sv"

module CPU_wrapper(input clk,
                   input rst,

                   // Master port -> axi bus
                   //WRITE ADDRESS
                   output logic [`AXI_ID_BITS-1:0]   AWID_M1,
                   output logic [`AXI_ADDR_BITS-1:0] AWADDR_M1,
                   output logic [`AXI_LEN_BITS-1:0]  AWLEN_M1,
                   output logic [`AXI_SIZE_BITS-1:0] AWSIZE_M1,
                   output logic [1:0]                AWBURST_M1,
                   output logic                      AWVALID_M1,
                   input  logic                      AWREADY_M1,
                   //WRITE DATA
                   output logic [`AXI_DATA_BITS-1:0] WDATA_M1,
                   output logic [`AXI_STRB_BITS-1:0] WSTRB_M1,
                   output logic                      WLAST_M1,
                   output logic                      WVALID_M1,
                   input  logic                      WREADY_M1,
                   //WRITE RESPONSE
                   input  logic [`AXI_ID_BITS-1:0] BID_M1,
                   input  logic [1:0]              BRESP_M1,
                   input  logic                    BVALID_M1,
                   output logic                    BREADY_M1,
                   //READ ADDRESS1
                   output logic [`AXI_ID_BITS-1:0]   ARID_M1,
                   output logic [`AXI_ADDR_BITS-1:0] ARADDR_M1,
                   output logic [`AXI_LEN_BITS-1:0]  ARLEN_M1,
                   output logic [`AXI_SIZE_BITS-1:0] ARSIZE_M1,
                   output logic [1:0]                ARBURST_M1,
                   output logic                      ARVALID_M1,
                   input  logic                      ARREADY_M1,
                   //READ DATA1
                   input  logic [`AXI_ID_BITS-1:0]   RID_M1,
                   input  logic [`AXI_DATA_BITS-1:0] RDATA_M1,
                   input  logic [1:0]                RRESP_M1,
                   input  logic                      RLAST_M1,
                   input  logic                      RVALID_M1,
                   output logic                      RREADY_M1,

                   //READ ADDRESS0
                   output logic [`AXI_ID_BITS-1:0]   ARID_M0,
                   output logic [`AXI_ADDR_BITS-1:0] ARADDR_M0,
                   output logic [`AXI_LEN_BITS-1:0]  ARLEN_M0,
                   output logic [`AXI_SIZE_BITS-1:0] ARSIZE_M0,
                   output logic [1:0]                ARBURST_M0,
                   output logic                      ARVALID_M0,
                   input  logic                      ARREADY_M0,
                   //READ DATA0
                   input  logic [`AXI_ID_BITS-1:0]   RID_M0,
                   input  logic [`AXI_DATA_BITS-1:0] RDATA_M0,
                   input  logic [1:0]                RRESP_M0,
                   input  logic                      RLAST_M0,
                   input  logic                      RVALID_M0,
                   output logic                      RREADY_M0
                 );

// Stall cpu signal
logic wrp2cpu_stall_m0; // Stall be4 instruction fetch stage
logic wrp2cpu_stall_m1; // Stall be4 memory write back
logic wrp2cpu_stall; // Stall be4 memory write back

assign wrp2cpu_stall = wrp2cpu_stall_m0 | wrp2cpu_stall_m1;

// Memory cs & oe 
logic                  im1_cs;
logic                  im1_oe;
logic [`WORD_SIZE-1:0] im1_di;
logic [3:0]            im1_web;
logic [`WORD_SIZE-1:0] im1_addr;
logic                  dm1_cs;
logic                  dm1_oe;
logic [3:0]            dm1_web;
logic [`WORD_SIZE-1:0] dm1_addr;
logic [`WORD_SIZE-1:0] dm1_data;

logic [`PC_SIZE-1:0] wrp2cpu_pc;
logic                wrp2cpu_rready;


logic [`WORD_SIZE-1:0] wrp2cpu_instr_hold;
logic [`WORD_SIZE-1:0] wrp2cpu_rdata_hold;

oska_cpu OSKA_CPU(.clk(clk),
                  .rst(rst),

                  // CPU wrapper to internal cpu
                  .wrp2cpu_stall  (wrp2cpu_stall),
                  .wrp2cpu_pc     (wrp2cpu_pc),
                  .wrp2cpu_rready (wrp2cpu_rready),


                  // Data memory port
                  .cpu2dm1_cs  (dm1_cs   ),
                  .cpu2dm1_oe  (dm1_oe   ),
                  .cpu2dm1_web (dm1_web  ),
                  .cpu2dm1_addr(dm1_addr ),
                  .cpu2dm1_di  (dm1_data ),
                  .dm12cpu_do  (wrp2cpu_rdata_hold),

                  // Instruction memory
                  .ifu2wrp_cs  (im1_cs   ),
                  .ifu2wrp_oe  (im1_oe   ),
                  .ifu2wrp_web (im1_web  ),
                  .cpu2im1_addr(im1_addr ),
                  .cpu2im1_di  (im1_di   ),
                  .im12cpu_do  (wrp2cpu_instr_hold)

                  );


// Fix assignment (might need to change in future)
//**ID assignments for each ports
assign AWID_M1 = `M1_ID_WADDR;
assign BID_M1  = `M1_ID_WRESP;
assign ARID_M1 = `M1_ID_RADDR;
assign RID_M1  = `M1_ID_RDATA;
assign ARID_M0 = `M0_ID_RADDR;
assign RID_M0  = `M0_ID_RDATA;

//**LEN assignments (only support burst mode 1 in this project)
assign AWLEN_M1 = `AXI_LEN_ONE;
assign ARLEN_M1 = `AXI_LEN_ONE;
assign ARLEN_M0 = `AXI_LEN_ONE;

//**SIZE assignments (burst size 1)
assign AWSIZE_M1 = `AXI_SIZE_BITS'b1;
assign ARSIZE_M1 = `AXI_SIZE_BITS'b1;
assign ARSIZE_M0 = `AXI_SIZE_BITS'b1;

//*BURST assignment
assign ARBURST_M0 = `AXI_BURST_INC;
assign ARBURST_M1 = `AXI_BURST_INC;
assign AWBURST_M1 = `AXI_BURST_INC;



assign WLAST_M1 = 1'b1;


// Finite state machine
logic [1:0] axi_m0_state_cur;
logic [2:0] axi_m1_state_cur;
logic [1:0] axi_m0_state_next;
logic [2:0] axi_m1_state_next;
logic       m1_addr;

logic [`WORD_SIZE-1:0] cpu2im1_addr_hold;
logic [`WORD_SIZE-1:0] cpu2dm1_addr_hold;
logic [`WORD_SIZE-1:0] cpu2dm1_data_hold;
logic [3:0]            cpu2dm1_web_hold;


assign wrp2cpu_pc     = cpu2im1_addr_hold;
assign wrp2cpu_rready = RREADY_M0;

always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    axi_m0_state_cur <= `M0_IDLE;
  end
  else begin
    axi_m0_state_cur <= axi_m0_state_next;
  end
end

assign m1_addr = (dm1_web == 4'b1111)? 1'b0: 1'b1;

always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    cpu2im1_addr_hold <= `IM_ADDR_SIZE'b0;
  end
  else begin
    case(axi_m0_state_cur)
      `M0_IDLE:cpu2im1_addr_hold <= (im1_oe )? im1_addr : `IM_ADDR_SIZE'b0;
      `M0_ADDR:cpu2im1_addr_hold <= cpu2im1_addr_hold;
      `M0_DATA:cpu2im1_addr_hold <= cpu2im1_addr_hold;
    endcase
  end
end

assign ARADDR_M0 = cpu2im1_addr_hold;

// State change for M0 instruction memory
always_comb begin
  case(axi_m0_state_cur)
    `M0_IDLE:axi_m0_state_next = (im1_oe    )? `M0_ADDR :`M0_IDLE;
    `M0_ADDR:axi_m0_state_next = (ARREADY_M0)? `M0_DATA :`M0_ADDR;
    `M0_DATA:axi_m0_state_next = (RVALID_M0 & wrp2cpu_stall_m1)? `M0_WAIT:
                                 (RVALID_M0 & ~wrp2cpu_stall_m1)?
                                 `M0_IDLE: `M0_DATA;
    `M0_WAIT:axi_m0_state_next = (axi_m1_state_cur == `M1_IDLE |
                                  axi_m1_state_cur == `M1_WAIT)?
                                  `M0_IDLE: `M0_WAIT;
  endcase
end

always_ff @(posedge clk, posedge rst) begin
  if (rst) begin
    wrp2cpu_instr_hold <= `WORD_SIZE'b0;
  end
  else if (RVALID_M0) begin
    wrp2cpu_instr_hold <= RDATA_M0;
  end
  else begin
    wrp2cpu_instr_hold <= wrp2cpu_instr_hold;
  end
end

always_comb begin
  case(axi_m0_state_cur)
    `M0_IDLE: begin
      wrp2cpu_stall_m0 = 1'b0;
      ARVALID_M0       = 1'b0;
      RREADY_M0        = 1'b1;
    end
    `M0_ADDR: begin
      wrp2cpu_stall_m0 = 1'b1;
      ARVALID_M0       = 1'b1;
      RREADY_M0        = 1'b0;
    end
    `M0_DATA: begin
      wrp2cpu_stall_m0 = 1'b1;
      ARVALID_M0       = 1'b0;
      RREADY_M0        = 1'b0;
    end
    `M0_WAIT: begin
      wrp2cpu_stall_m0 = 1'b1;
      ARVALID_M0       = 1'b0;
      RREADY_M0        = 1'b1;
    end
  endcase
end

// Master 1
always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    axi_m1_state_cur <= `M1_IDLE;
  end
  else begin
    axi_m1_state_cur <= axi_m1_state_next;
  end
end

always_ff @(posedge clk, posedge rst) begin
  if (rst) begin
    wrp2cpu_rdata_hold <= `WORD_SIZE'b0;
  end
  else if (RVALID_M1) begin
    wrp2cpu_rdata_hold <= RDATA_M1;
  end
  else begin
    wrp2cpu_rdata_hold <= wrp2cpu_rdata_hold;
  end
end

always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    cpu2dm1_addr_hold <= `IM_ADDR_SIZE'b0;
  end
  else begin
    case(axi_m1_state_cur)
      `M1_IDLE: cpu2dm1_addr_hold <= (dm1_oe | m1_addr)? dm1_addr: `IM_ADDR_SIZE'b0;
      `M1_ADDR: cpu2dm1_addr_hold <= cpu2dm1_addr_hold;
      `M1_DATA: cpu2dm1_addr_hold <= cpu2dm1_addr_hold;
      `M1_WADDR:cpu2dm1_addr_hold <= cpu2dm1_addr_hold;
      `M1_WDATA:cpu2dm1_addr_hold <= cpu2dm1_addr_hold;
      `M1_WRESP:cpu2dm1_addr_hold <= `IM_ADDR_SIZE'b0;
    endcase
  end
end

always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    cpu2dm1_data_hold <= `IM_ADDR_SIZE'b0;
  end
  else begin
    case(axi_m1_state_cur)
      `M1_IDLE: cpu2dm1_data_hold <= (m1_addr)? dm1_data: `IM_ADDR_SIZE'b0;
      `M1_ADDR: cpu2dm1_data_hold <= `IM_ADDR_SIZE'b0;
      `M1_DATA: cpu2dm1_data_hold <= `IM_ADDR_SIZE'b0;
      `M1_WADDR:cpu2dm1_data_hold <= cpu2dm1_data_hold;
      `M1_WDATA:cpu2dm1_data_hold <= cpu2dm1_data_hold;
      `M1_WRESP:cpu2dm1_data_hold <= cpu2dm1_data_hold;
    endcase
  end
end

always_ff @(posedge clk, posedge rst)
begin
  if(rst) begin
    cpu2dm1_web_hold <= `IM_ADDR_SIZE'b0;
  end
  else begin
    case(axi_m1_state_cur)
      `M1_IDLE: cpu2dm1_web_hold <= dm1_web;
      default : cpu2dm1_web_hold <= cpu2dm1_web_hold;
    endcase
  end
end

assign WSTRB_M1 = cpu2dm1_web_hold;
assign WDATA_M1 = cpu2dm1_data_hold;
assign ARADDR_M1 = cpu2dm1_addr_hold;
assign AWADDR_M1 = cpu2dm1_addr_hold;

// State change for M1 data memory
always_comb begin
  case(axi_m1_state_cur)
    `M1_IDLE :axi_m1_state_next = (dm1_oe)?  `M1_ADDR:
                                  (m1_addr)? `M1_WADDR  : `M1_IDLE;
    `M1_ADDR :axi_m1_state_next = (ARREADY_M1)? `M1_DATA: `M1_ADDR;
    `M1_DATA :axi_m1_state_next = (RVALID_M1 & wrp2cpu_stall_m0)? `M1_WAIT:
                                  (RVALID_M1 & ~wrp2cpu_stall_m0)?
                                  `M1_IDLE: `M1_DATA;
    `M1_WADDR:axi_m1_state_next = (AWREADY_M1)? `M1_WDATA: `M1_WADDR;
    `M1_WDATA:axi_m1_state_next = (WREADY_M1 )? `M1_WRESP: `M1_WDATA;
    `M1_WRESP:axi_m1_state_next = (BVALID_M1 & wrp2cpu_stall_m0)? `M1_WAIT:
                                  (BVALID_M1 & ~wrp2cpu_stall_m0)?
                                  `M1_IDLE: `M1_WRESP;
    `M1_WAIT :axi_m1_state_next = (axi_m0_state_cur == `M0_IDLE |
                                   axi_m0_state_cur == `M0_WAIT)?
                                   `M1_IDLE: `M1_WAIT;
  endcase
end

always_comb begin
  case(axi_m1_state_cur)
    `M1_IDLE:
    begin
      wrp2cpu_stall_m1 = 1'b0;
      ARVALID_M1       = 1'b0;
      RREADY_M1        = 1'b1;
      AWVALID_M1       = 1'b0;
      WVALID_M1        = 1'b0;
      BREADY_M1        = 1'b1;
    end
    `M1_ADDR:
    begin
      wrp2cpu_stall_m1 = 1'b1;
      ARVALID_M1       = 1'b1;
      RREADY_M1        = 1'b0;
      AWVALID_M1       = 1'b0;
      WVALID_M1        = 1'b0;
      BREADY_M1        = 1'b0;
    end
    `M1_DATA:
    begin
      wrp2cpu_stall_m1 = 1'b1;
      ARVALID_M1       = 1'b0;
      RREADY_M1        = 1'b0;
      AWVALID_M1       = 1'b0;
      WVALID_M1        = 1'b0;
      BREADY_M1        = 1'b0;
    end
    `M1_WADDR:
    begin
      wrp2cpu_stall_m1 = 1'b1;
      ARVALID_M1       = 1'b0;
      RREADY_M1        = 1'b0;
      AWVALID_M1       = 1'b1;
      WVALID_M1        = 1'b0;
      BREADY_M1        = 1'b0;
    end
    `M1_WDATA:
    begin
      wrp2cpu_stall_m1 = 1'b1;
      ARVALID_M1       = 1'b0;
      RREADY_M1        = 1'b0;
      AWVALID_M1       = 1'b0;
      WVALID_M1        = 1'b1;
      BREADY_M1        = 1'b0;
    end
    `M1_WRESP:
    begin
      wrp2cpu_stall_m1 = 1'b1;
      ARVALID_M1       = 1'b0;
      RREADY_M1        = 1'b0;
      AWVALID_M1       = 1'b0;
      WVALID_M1        = 1'b0;
      BREADY_M1        = 1'b0;
    end
    `M1_WAIT:
    begin
      wrp2cpu_stall_m1 = 1'b1;
      ARVALID_M1       = 1'b0;
      RREADY_M1        = 1'b1;
      AWVALID_M1       = 1'b0;
      WVALID_M1        = 1'b0;
      BREADY_M1        = 1'b1;
    end
  endcase
end


endmodule
