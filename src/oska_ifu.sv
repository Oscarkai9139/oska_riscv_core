// Author  : Oscarkai9139
// Project : gitlab.com/oscarkai9139/oska_riscv_core.git
// File    : oska_ifu.sv

`include "oska_define.sv"
`include "oska_pc.sv"

module oska_ifu(input logic clk,
                input logic rst,

                // Input instruction fetch from wrapper
                input logic [`WORD_SIZE-1:0] wrp2ifu_instr,
                input logic [`PC_SIZE-1:0]   wrp2cpu_pc,
                input logic                  wrp2cpu_rready,

                // Stall from wrapper cpu
                input logic wrp2cpu_stall,

                // Input Jump addr from EXU
                // PC <--- IFU <--- EXU
                input logic                idu2pc_stall,
                input logic [`PC_SIZE-1:0] exu2pc_jump_addr,
                input logic                exu2pc_jump,

                // Output from PC module
                // PC ---> IFU ---> IM & IDU
                output logic [`PC_SIZE-1:0] ifu2im1_pc_next,
                output logic [`IM_WEB_SIZE-1:0] ifu2wrp_web,
                output logic                    ifu2wrp_cs,
                output logic                    ifu2wrp_oe,

                // Output pipeline regs
                // IFU ---> IDU
                output logic [`PC_SIZE-1:0]   ifu2idu_pr_pc,
                output logic [`WORD_SIZE-1:0] ifu2idu_pr_instr,
                output logic                  ifu2idu_pr_instr_valid);

  oska_pc OSKA_PC(.clk(clk),
                  .rst(rst),
                  .wrp2cpu_stall   (wrp2cpu_stall   ),
                  .idu2pc_stall    (idu2pc_stall    ),
                  .exu2pc_jump     (exu2pc_jump     ),
                  .exu2pc_jump_addr(exu2pc_jump_addr),
                  .pc2ifu_pc_next  (ifu2im1_pc_next ));


  assign ifu2wrp_web = 4'b1111;
  assign ifu2wrp_cs  = `ENABLE;
  assign ifu2wrp_oe  = `ENABLE;

  //logic  ifu_im_oe;
  //assign ifu_im_oe = (exu2pc_jump)? `DISABLE : `ENABLE;

  // Make the next instruction invalid when jump happens
  logic  ifu_instr_valid;
  assign ifu_instr_valid = (wrp2cpu_rready == 1'b0)? `INVALID:
                           (exu2pc_jump )? `INVALID:
                           (idu2pc_stall)? `INVALID: `VALID;

  logic load_delay_counter;

  always_ff @(posedge clk, posedge rst) begin
    if(rst) begin
      load_delay_counter <= 1'b0;
    end
    else if (wrp2cpu_stall) begin
      load_delay_counter <= load_delay_counter;
    end
    else begin
      load_delay_counter <= (idu2pc_stall)? 1'b1: 1'b0;
    end
  end



  // Output Pipeline regs IF/ID
  always_ff @(posedge clk, posedge rst)
  begin
    if(rst) begin
      ifu2idu_pr_instr_valid <= `INVALID;
      ifu2idu_pr_instr       <= `WORD_SIZE'b0;
      ifu2idu_pr_pc          <= `PC_SIZE'b0;
    end
    else if (wrp2cpu_stall) begin
      ifu2idu_pr_instr_valid <= ifu2idu_pr_instr_valid;
      ifu2idu_pr_instr       <= ifu2idu_pr_instr;
      ifu2idu_pr_pc          <= ifu2idu_pr_pc;
    end
    else begin
      ifu2idu_pr_instr_valid <= ifu_instr_valid;
      ifu2idu_pr_instr       <= (load_delay_counter)?
                                ifu2idu_pr_instr: wrp2ifu_instr;
      ifu2idu_pr_pc          <= (load_delay_counter)?
                                ifu2idu_pr_pc: wrp2cpu_pc;
    end
  end

endmodule
