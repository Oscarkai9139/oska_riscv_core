`include "AXI_define.svh"

module SRAM_wrapper (
  input wire ACLK,
  input wire ARESETn,
  //AR_channel
  input wire [`AXI_IDS_BITS-1:0] ARID,
  input wire [`AXI_ADDR_BITS-1:0] ARADDR,
  input wire [`AXI_LEN_BITS-1:0] ARLEN,
  input wire [`AXI_SIZE_BITS-1:0] ARSIZE,
  input wire [`AXI_BURST_BITS-1:0] ARBURST,
  input wire ARVALID,
  output reg ARREADY,
  //R_channel
  input wire RREADY,
  output reg [`AXI_IDS_BITS-1:0] RID,
  output wire [`AXI_DATA_BITS-1:0] RDATA,
  output reg [`AXI_RESP_BITS-1:0] RRESP,
  output reg RLAST,
  output reg RVALID,
  //AW_channel
  input wire [`AXI_IDS_BITS-1:0] AWID,
  input wire [`AXI_ADDR_BITS-1:0] AWADDR,
  input wire [`AXI_LEN_BITS-1:0] AWLEN,
  input wire [`AXI_SIZE_BITS-1:0] AWSIZE,
  input wire [`AXI_BURST_BITS-1:0] AWBURST,
  input wire AWVALID,
  output reg AWREADY,
  //W_channel
  input wire [`AXI_DATA_BITS-1:0] WDATA,
  input wire [`AXI_STRB_BITS-1:0] WSTRB,
  input wire WLAST,
  input wire WVALID,
  output reg WREADY,
  //B_channel
  input wire BREADY,
  output reg [`AXI_IDS_BITS-1:0] BID,
  output reg [`AXI_RESP_BITS-1:0] BRESP,
  output reg BVALID
);

  //==========State Control==========\\
  //Request interface control
  localparam IDLE  = 2'b00;
  localparam READ  = 2'b01;
  localparam WRITE = 2'b10;
  reg [1:0]                 request_type;
  reg [`AXI_IDS_BITS-1:0]   request_id;
  reg [`AXI_ADDR_BITS-1:0]  request_addr;
  reg [`AXI_LEN_BITS-1:0]   request_len;
  reg [`AXI_SIZE_BITS-1:0]  request_size;
  reg [`AXI_BURST_BITS-1:0] request_burst;
  reg [`AXI_DATA_BITS-1:0]  request_data;
  reg [`AXI_STRB_BITS-1:0]  request_strb;

  reg                       dual_request;
  reg [`AXI_IDS_BITS-1:0]   dual_id;
  reg [`AXI_ADDR_BITS-1:0]  dual_addr;
  reg [`AXI_LEN_BITS-1:0]   dual_len;
  reg [`AXI_SIZE_BITS-1:0]  dual_size;
  reg [`AXI_BURST_BITS-1:0] dual_burst;
  
  //==========SRAM Signals==========\\
  wire CS, OE;
  reg [`AXI_STRB_BITS-1:0] WEB;
  reg [13:0] A;
  reg [`AXI_DATA_BITS-1:0] DI;
  wire [`AXI_DATA_BITS-1:0] DO;

  always_ff @(posedge ACLK or negedge ARESETn) begin
    unique if (ARESETn) begin
      unique if (dual_request) begin
        if (WVALID) begin
          dual_request  <= 1'b0;
          WREADY        <= 1'b0;
          request_type  <= WRITE;
          request_id    <= dual_id;
          request_addr  <= dual_addr;
          request_len   <= dual_len;
          request_size  <= dual_size;
          request_burst <= dual_burst;
          request_data  <= WDATA;
          request_strb  <= WSTRB;
        end else begin
          request_type  <= IDLE;
        end
      end else if(ARREADY || AWREADY || WREADY) begin
        //Idle
        if ((ARREADY & ARVALID) && (AWREADY & AWVALID)) begin
          //Request RW at the same time
          //Handle read first
          dual_request  <= 1'b1;
          dual_id       <= AWID;
          dual_addr     <= AWADDR;
          dual_len      <= AWLEN;
          dual_size     <= AWSIZE;
          dual_burst    <= AWBURST;

          ARREADY       <= 1'b0;
          AWREADY       <= 1'b0;
          WREADY        <= 1'b1;
          request_type  <= READ;
          request_id    <= ARID;
          request_addr  <= ARADDR;
          request_len   <= ARLEN;
          request_size  <= ARSIZE;
          request_burst <= ARBURST;
        end else if (ARREADY & ARVALID) begin
          ARREADY       <= 1'b0;
          AWREADY       <= 1'b0;
          WREADY        <= 1'b0;
          request_type  <= READ;
          request_id    <= ARID;
          request_addr  <= ARADDR;
          request_len   <= ARLEN;
          request_size  <= ARSIZE;
          request_burst <= ARBURST;
        end else if (AWREADY & AWVALID) begin
          ARREADY       <= 1'b0;
          AWREADY       <= 1'b0;
          WREADY        <= 1'b1;
          request_id    <= AWID;
          request_addr  <= AWADDR;
          request_len   <= AWLEN;
          request_size  <= AWSIZE;
          request_burst <= AWBURST;
          request_type  <= IDLE;
        end else if (WREADY & WVALID) begin
          WREADY        <= 1'b0;
          request_type  <= WRITE;
          request_data  <= WDATA;
          request_strb  <= WSTRB;
        end else begin
          //No requests happen
          request_type  <= IDLE;
        end
      end else begin
        request_type <= IDLE;
        if (RVALID && RREADY) begin
          if(request_type==IDLE && !BVALID) begin
            ARREADY       <= 1'b1;
            AWREADY       <= 1'b1;
            WREADY        <= 1'b0;
          end
        end
        if (BVALID && BREADY) begin
          if (!RVALID) begin
            ARREADY       <= 1'b1;
            AWREADY       <= 1'b1;
            WREADY        <= 1'b0;
          end
        end
      end
    end else begin
      //Reset
      ARREADY <= 1'b1;
      AWREADY <= 1'b1;
      WREADY  <= 1'b0;

      request_type  <= IDLE;
      request_id    <= {`AXI_IDS_BITS{1'b0}};
      request_addr  <= {`AXI_ADDR_BITS{1'b0}};
      request_len   <= {`AXI_LEN_BITS{1'b0}};
      request_size  <= {`AXI_SIZE_BITS{1'b0}};
      request_burst <= {`AXI_BURST_BITS{1'b0}};
      request_data  <= {`AXI_DATA_BITS{1'b0}};
      request_strb  <= {`AXI_STRB_BITS{1'b0}};
      dual_request  <= 1'b0;
      dual_id       <= {`AXI_IDS_BITS{1'b0}};
      dual_addr     <= {`AXI_ADDR_BITS{1'b0}};
      dual_len      <= {`AXI_LEN_BITS{1'b0}};
      dual_size     <= {`AXI_SIZE_BITS{1'b0}};
      dual_burst    <= {`AXI_BURST_BITS{1'b0}};
    end
  end

  //Response interface control
  assign RLAST = 1'b1;
  assign RDATA = DO;
  always_ff @(posedge ACLK or negedge ARESETn) begin
    unique if (ARESETn) begin
      //Set response
      unique case (request_type)
        IDLE : begin
          //Do not update any output
        end
        READ : begin
          RID    <= request_id;
          //RDATA  <= DO;
          RRESP  <= `AXI_RESP_OKAY;
          RVALID <= 1'b1;
        end
        WRITE : begin
          BID    <= request_id;
          BRESP  <= `AXI_RESP_OKAY;
          BVALID <= 1'b1;
        end
        default: begin
        end
      endcase
      //Check if bridge get it
      if (RVALID & RREADY) begin
        RVALID <= 1'b0;
      end
      if (BVALID & BREADY) begin
        BVALID <= 1'b0;
      end
    end else begin
      //Reset
      RID    <= {`AXI_IDS_BITS{1'b0}};
      RRESP  <= {`AXI_RESP_BITS{1'b0}};
      RVALID <= 1'b0;
      BID    <= {`AXI_IDS_BITS{1'b0}};
      BRESP  <= {`AXI_RESP_BITS{1'b0}};
      BVALID <= 1'b0;
    end
  end

  //SRAM signal control
  assign CS = 1'b1;
  assign OE = 1'b1;
  
  always_comb begin
    unique case (request_type)
      IDLE : begin
        //Do nothing
      end
      READ : begin
        WEB = {`AXI_STRB_BITS{1'b1}}; //Active low for SRAM
        A   = request_addr[15:2];
        DI  = {`AXI_DATA_BITS{1'b0}};
      end
      WRITE : begin
        WEB = request_strb;
        A   = request_addr[15:2];
        DI  = request_data;
      end
      default: begin
      end
    endcase
  end

  //=========Module Connect==========\\
  SRAM i_SRAM (
    .A0   (A[0]  ),
    .A1   (A[1]  ),
    .A2   (A[2]  ),
    .A3   (A[3]  ),
    .A4   (A[4]  ),
    .A5   (A[5]  ),
    .A6   (A[6]  ),
    .A7   (A[7]  ),
    .A8   (A[8]  ),
    .A9   (A[9]  ),
    .A10  (A[10] ),
    .A11  (A[11] ),
    .A12  (A[12] ),
    .A13  (A[13] ),
    .DO0  (DO[0] ),
    .DO1  (DO[1] ),
    .DO2  (DO[2] ),
    .DO3  (DO[3] ),
    .DO4  (DO[4] ),
    .DO5  (DO[5] ),
    .DO6  (DO[6] ),
    .DO7  (DO[7] ),
    .DO8  (DO[8] ),
    .DO9  (DO[9] ),
    .DO10 (DO[10]),
    .DO11 (DO[11]),
    .DO12 (DO[12]),
    .DO13 (DO[13]),
    .DO14 (DO[14]),
    .DO15 (DO[15]),
    .DO16 (DO[16]),
    .DO17 (DO[17]),
    .DO18 (DO[18]),
    .DO19 (DO[19]),
    .DO20 (DO[20]),
    .DO21 (DO[21]),
    .DO22 (DO[22]),
    .DO23 (DO[23]),
    .DO24 (DO[24]),
    .DO25 (DO[25]),
    .DO26 (DO[26]),
    .DO27 (DO[27]),
    .DO28 (DO[28]),
    .DO29 (DO[29]),
    .DO30 (DO[30]),
    .DO31 (DO[31]),
    .DI0  (DI[0] ),
    .DI1  (DI[1] ),
    .DI2  (DI[2] ),
    .DI3  (DI[3] ),
    .DI4  (DI[4] ),
    .DI5  (DI[5] ),
    .DI6  (DI[6] ),
    .DI7  (DI[7] ),
    .DI8  (DI[8] ),
    .DI9  (DI[9] ),
    .DI10 (DI[10]),
    .DI11 (DI[11]),
    .DI12 (DI[12]),
    .DI13 (DI[13]),
    .DI14 (DI[14]),
    .DI15 (DI[15]),
    .DI16 (DI[16]),
    .DI17 (DI[17]),
    .DI18 (DI[18]),
    .DI19 (DI[19]),
    .DI20 (DI[20]),
    .DI21 (DI[21]),
    .DI22 (DI[22]),
    .DI23 (DI[23]),
    .DI24 (DI[24]),
    .DI25 (DI[25]),
    .DI26 (DI[26]),
    .DI27 (DI[27]),
    .DI28 (DI[28]),
    .DI29 (DI[29]),
    .DI30 (DI[30]),
    .DI31 (DI[31]),
    .CK   (ACLK    ),
    .WEB0 (WEB[0]),
    .WEB1 (WEB[1]),
    .WEB2 (WEB[2]),
    .WEB3 (WEB[3]),
    .OE   (OE    ),
    .CS   (CS    )
  );
endmodule
