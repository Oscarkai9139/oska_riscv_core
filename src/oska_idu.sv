`include "oska_define.sv"
`include "oska_register_file.sv"

module oska_idu(input logic clk,
                input logic rst,

                // Wrapper to internal cpu
                input logic wrp2cpu_stall,

                // Inputs from IFU & IM1
                input  logic [`WORD_SIZE-1:0] ifu2idu_pr_instr,
                input  logic                  ifu2idu_pr_instr_valid,
                input  logic [`PC_SIZE-1:0]   ifu2idu_pr_pc,

                // Inputs from WBU for regs write back
                input  logic                     wbu2rf_reg_web,
                input  logic [`ADDR_SIZE-1:0]    wbu2rf_reg_addr,
                input  logic [`REG_DATASIZE-1:0] wbu2rf_reg_data,

                // Input Flush signals connect to jump signal
                input  logic                     ctrl_flush_idu,

                input  logic [`ADDR_SIZE-1:0]  mem2idu_reg_addr,
                input  logic                   mem2idu_reg_web,
                output logic                   idu2exu_wbu_fwd_reg_sel1,
                output logic                   idu2exu_wbu_fwd_reg_sel2,

                // Outputs stall to pc
                output logic                   idu2pc_stall,

                // Outputs pipeline regs
                // IDU ---> EXU
                output logic [`PC_SIZE-1:0]    idu2exu_pr_pc,
                output logic [`WORD_SIZE-1:0]  idu2exu_pr_imme,
                output logic                   idu2exu_pr_sign, //instr[30]
                output logic [`ADDR_SIZE-1:0]  idu2exu_pr_rs1_id,
                output logic [`ADDR_SIZE-1:0]  idu2exu_pr_rs2_id,
                output logic [`WORD_SIZE-1:0]  idu2exu_pr_rs1,
                output logic [`WORD_SIZE-1:0]  idu2exu_pr_rs2,  //imme shamt
                output logic [`ALUOP_SIZE-1:0] idu2exu_pr_aluop,
                output logic [3:0]             idu2exu_pr_optype,
                output logic [`ADDR_SIZE-1:0]  idu2exu_pr_rd_addr,

                // Outputs pipeline regs (control signals)
                // IDU ---> EXU
                output logic                    idu2exu_pr_flush,
                output logic                    idu2exu_pr_byte,
                output logic                    idu2exu_pr_half,
                output logic                    idu2exu_pr_unsigned,
                output logic                    idu2exu_pr_instr_valid,
                output logic [`DM_WEB_SIZE-1:0] idu2exu_pr_mem_web,
                output logic                    idu2exu_pr_mem_oe,
                output logic                    idu2exu_pr_reg_web,
                output logic                    idu2exu_pr_mem2reg);

  // Current instruction
  logic [`WORD_SIZE-1:0] instr;

  // Instruction mask for Rtype
  logic [`OP_SIZE-1:0  ] imask_op;
  logic                  imask_sign;
  logic [`ADDR_SIZE-1:0] imask_rs1_id;
  logic [`ADDR_SIZE-1:0] imask_rs2_id;
  logic [`ADDR_SIZE-1:0] imask_rd_id;
  logic [`ALUOP_SIZE-1:0] imask_aluop;

  // Declaration for immediate generator
  logic [`WORD_SIZE-1:0] imme_itype;
  logic [`WORD_SIZE-1:0] imme_stype;
  logic [`WORD_SIZE-1:0] imme_btype;
  logic [`WORD_SIZE-1:0] imme_utype;
  logic [`WORD_SIZE-1:0] imme_jtype;
  logic [`WORD_SIZE-1:0] imme_itype_u;
  logic [`WORD_SIZE-1:0] imme_stype_u;
  logic [`WORD_SIZE-1:0] imme_btype_u;
  logic [`WORD_SIZE-1:0] imme_utype_u;
  logic [`WORD_SIZE-1:0] imme_jtype_u;

  // Inputs assignment
  assign instr  = (ifu2idu_pr_instr_valid == `VALID)? ifu2idu_pr_instr: 32'h0;

  assign imask_op     = ifu2idu_pr_instr[6:0];
  assign imask_sign   = instr[30];
  assign imask_rs1_id = instr[19:15];
  assign imask_rs2_id = instr[24:20];
  assign imask_rd_id  = instr[11:7 ];
  assign imask_aluop  = instr[14:12];

  // Temporary hold value wires declaration
  logic [`WORD_SIZE-1:0]  idu_imme_hold;
  logic [`ADDR_SIZE-1:0]  idu_rs1_id;
  logic [`ADDR_SIZE-1:0]  idu_rs2_id;
  logic [`WORD_SIZE-1:0]  idu_rs1_rdata;
  logic [`WORD_SIZE-1:0]  idu_rs2_rdata;
  logic [`WORD_SIZE-1:0]  idu_rs1_rdata_hold;
  logic [`WORD_SIZE-1:0]  rs2_itype_hold;
  logic [`WORD_SIZE-1:0]  idu_rs2_rdata_hold;
  logic [`ADDR_SIZE-1:0]  idu_rd_waddr_hold;
  logic [3:0]             idu_optype_hold;
  logic [`ALUOP_SIZE-1:0] idu_aluop_hold;
  logic                   idu_byte;
  logic                   idu_half;
  logic                   idu_unsigned;
  logic                   idu_itype_sign;
  logic                   idu_sign;
  logic                   idu_instr_valid;

  logic [`DM_WEB_SIZE-1:0] idu_mem_web;
  logic                    idu_mem_oe;
  logic                    idu_reg_web;
  logic                    idu_mem2reg;

  // Declaration of Control signals
  logic [`DM_WEB_SIZE-1:0] idu_mem_web_hold;
  logic                    idu_mem_oe_hold;
  logic                    idu_reg_web_hold;
  logic                    idu_mem2reg_hold;

  // Flush control signal when jump taken
  assign idu_mem_web_hold = (ctrl_flush_idu | ~ifu2idu_pr_instr_valid)?
                            `MEM_READ : idu_mem_web;
  assign idu_mem_oe_hold  = (ctrl_flush_idu | ~ifu2idu_pr_instr_valid)?
                            `DISABLE  : idu_mem_oe;
  assign idu_reg_web_hold = (ctrl_flush_idu | ~ifu2idu_pr_instr_valid)?
                            `REG_READ : idu_reg_web;
  assign idu_mem2reg_hold = (ctrl_flush_idu | ~ifu2idu_pr_instr_valid)?
                            1'b0 : idu_mem2reg;

  assign idu2pc_stall = (~ifu2idu_pr_instr_valid)? 1'b0:
                        (imask_op == `OP_ITYPE_LOAD)? 1'b1: 1'b0;

  // Validity instruction for jump & jump
  assign idu_instr_valid = (ctrl_flush_idu)? `INVALID: ifu2idu_pr_instr_valid;

  // Optype generator
  always_comb
  begin
    case(imask_op)
      `OP_RTYPE      : idu_optype_hold = `RTYPE;
      `OP_ITYPE_LOAD : idu_optype_hold = `ITYPE_LOAD;
      `OP_ITYPE_ALU  : idu_optype_hold = `ITYPE_ALU;
      `OP_ITYPE_JUMP : idu_optype_hold = `ITYPE_JUMP;
      `OP_STYPE      : idu_optype_hold = `STYPE;
      `OP_BTYPE      : idu_optype_hold = `BTYPE;
      `OP_UTYPE_AUIPC: idu_optype_hold = `UTYPE_AUIPC;
      `OP_UTYPE_LUI  : idu_optype_hold = `UTYPE_LUI;
      `OP_JTYPE      : idu_optype_hold = `JTYPE;
      default        : idu_optype_hold = `ERROR_TYPE;
    endcase
  end

  // Immediate Generator
  assign imme_itype = {{20{instr[31]}}, instr[31:20]};
  assign imme_stype = {{20{instr[31]}}, instr[31:25], instr[11:7]};//
  assign imme_btype = {{19{instr[31]}}, instr[31], instr[7], instr[30:25], instr[11:8], 1'b0};//
  assign imme_utype = {instr[31:12], 12'b0};//
  assign imme_jtype = {{12{instr[31]}}, instr[19:12], instr[20], instr[30:21], 1'b0};//

  assign imme_itype_u = {{20{1'b0}}, instr[31:20]};
  assign imme_stype_u = {{20{1'b0}}, instr[31:25], instr[11:7]};
  assign imme_btype_u = {{19{1'b0}}, instr[31], instr[7], instr[30:25], instr[11:8], 1'b0};
  assign imme_utype_u = {instr[31:12], 12'b0};
  assign imme_jtype_u = {{11{1'b0}}, instr[31], instr[19:12], instr[20], instr[30:21], 1'b0};

  // Imme for pc calculation only
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_imme_hold = `WORD_SIZE'b0;
      `OP_ITYPE_LOAD : idu_imme_hold = imme_itype;
      `OP_ITYPE_ALU  : idu_imme_hold = `WORD_SIZE'b0;
      `OP_ITYPE_JUMP : idu_imme_hold = imme_itype;
      `OP_STYPE      : idu_imme_hold = imme_stype;
      `OP_BTYPE      : idu_imme_hold = imme_btype;
      `OP_UTYPE_AUIPC: idu_imme_hold = imme_utype;
      `OP_UTYPE_LUI  : idu_imme_hold = imme_utype;
      `OP_JTYPE      : idu_imme_hold = imme_jtype;
      default        : idu_imme_hold = `WORD_SIZE'b0;
    endcase
  end

  logic fwd_wbu_reg_sel1;
  logic fwd_wbu_reg_sel2;

  assign fwd_wbu_reg_sel1 = (mem2idu_reg_web  == `REG_WRITE &
                             mem2idu_reg_addr != `REG_X0    &
                             idu_rs1_id   == mem2idu_reg_addr)?
                             `TRUE: `FALSE;
  assign fwd_wbu_reg_sel2 = (mem2idu_reg_web  == `REG_WRITE &
                             mem2idu_reg_addr != `REG_X0    &
                             idu_rs2_id   == mem2idu_reg_addr)?
                             `TRUE: `FALSE;

  logic [`WORD_SIZE-1:0] idu_fwd_rs1_rdata;
  logic [`WORD_SIZE-1:0] idu_fwd_rs2_rdata;

  assign idu_fwd_rs1_rdata = (wbu2rf_reg_web  == `REG_WRITE &
                              wbu2rf_reg_addr != `REG_X0    &
                              idu_rs1_id == wbu2rf_reg_addr)?
                              wbu2rf_reg_data: idu_rs1_rdata;
  assign idu_fwd_rs2_rdata = (wbu2rf_reg_web  == `REG_WRITE &
                              wbu2rf_reg_addr != `REG_X0    &
                              idu_rs2_id == wbu2rf_reg_addr)?
                              wbu2rf_reg_data: idu_rs2_rdata;

  // Hold register file write enable when cpu stall
  logic  rf_reg_web;
  assign rf_reg_web = (wrp2cpu_stall)? 1'b0: wbu2rf_reg_web;

  // RF module
  oska_register_file OSKA_RF(.clk(clk),
                             .rst(rst),
                             .reg1_raddr(idu_rs1_id   ),
                             .reg1_rdata(idu_rs1_rdata),
                             .reg2_raddr(idu_rs2_id   ),
                             .reg2_rdata(idu_rs2_rdata),
                             .reg1_web  (rf_reg_web   ),
                             .reg1_waddr(wbu2rf_reg_addr),
                             .reg1_wdata(wbu2rf_reg_data));

  // RS1 ID
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_rs1_id = imask_rs1_id;
      `OP_ITYPE_LOAD : idu_rs1_id = imask_rs1_id;
      `OP_ITYPE_ALU  : idu_rs1_id = imask_rs1_id;
      `OP_ITYPE_JUMP : idu_rs1_id = imask_rs1_id;
      `OP_STYPE      : idu_rs1_id = imask_rs1_id;
      `OP_BTYPE      : idu_rs1_id = imask_rs1_id;
      `OP_UTYPE_AUIPC: idu_rs1_id = `REG_X0;
      `OP_UTYPE_LUI  : idu_rs1_id = `REG_X0;
      `OP_JTYPE      : idu_rs1_id = `REG_X0;
      default        : idu_rs1_id = `REG_X0;
    endcase
  end

  // sign for itype alu
  always_comb begin
    case (imask_aluop)
      `ALUOP_ADD : idu_itype_sign = 1'b0;
      `ALUOP_SLL : idu_itype_sign = 1'b0;
      `ALUOP_SLT : idu_itype_sign = 1'b0;
      `ALUOP_SLTU: idu_itype_sign = 1'b0;
      `ALUOP_XOR : idu_itype_sign = 1'b0;
      `ALUOP_SRL : idu_itype_sign = imask_sign;
      `ALUOP_OR  : idu_itype_sign = 1'b0;
      `ALUOP_AND : idu_itype_sign = 1'b0;
    endcase
  end

  // SIGN
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_sign = imask_sign;
      `OP_ITYPE_LOAD : idu_sign = 1'b0;
      `OP_ITYPE_ALU  : idu_sign = idu_itype_sign;
      `OP_ITYPE_JUMP : idu_sign = 1'b0;
      `OP_STYPE      : idu_sign = 1'b0;
      `OP_BTYPE      : idu_sign = 1'b0;
      `OP_UTYPE_AUIPC: idu_sign = 1'b0;
      `OP_UTYPE_LUI  : idu_sign = 1'b0;
      `OP_JTYPE      : idu_sign = 1'b0;
      default        : idu_sign = 1'b0;
    endcase
  end


  // RS1
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_rs1_rdata_hold = idu_fwd_rs1_rdata;
      `OP_ITYPE_LOAD : idu_rs1_rdata_hold = idu_fwd_rs1_rdata;
      `OP_ITYPE_ALU  : idu_rs1_rdata_hold = idu_fwd_rs1_rdata;
      `OP_ITYPE_JUMP : idu_rs1_rdata_hold = idu_fwd_rs1_rdata;
      `OP_STYPE      : idu_rs1_rdata_hold = idu_fwd_rs1_rdata;
      `OP_BTYPE      : idu_rs1_rdata_hold = idu_fwd_rs1_rdata;
      `OP_UTYPE_AUIPC: idu_rs1_rdata_hold = `WORD_SIZE'b0;
      `OP_UTYPE_LUI  : idu_rs1_rdata_hold = `WORD_SIZE'b0;
      `OP_JTYPE      : idu_rs1_rdata_hold = `WORD_SIZE'b0;
      default        : idu_rs1_rdata_hold = `WORD_SIZE'b0;
    endcase
  end

  // RS2 ID
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_rs2_id = imask_rs2_id;
      `OP_ITYPE_LOAD : idu_rs2_id = `REG_X0;
      `OP_ITYPE_ALU  : idu_rs2_id = `REG_X0;
      `OP_ITYPE_JUMP : idu_rs2_id = `REG_X0;
      `OP_STYPE      : idu_rs2_id = imask_rs2_id;
      `OP_BTYPE      : idu_rs2_id = imask_rs2_id;
      `OP_UTYPE_AUIPC: idu_rs2_id = `REG_X0;
      `OP_UTYPE_LUI  : idu_rs2_id = `REG_X0;
      `OP_JTYPE      : idu_rs2_id = `REG_X0;
      default        : idu_rs2_id = `REG_X0;
    endcase
  end


  always_comb begin
    case (imask_aluop)
      `ALUOP_ADD : rs2_itype_hold = imme_itype;
      `ALUOP_SLL : rs2_itype_hold = imme_itype_u;
      `ALUOP_SLT : rs2_itype_hold = imme_itype;
      `ALUOP_SLTU: rs2_itype_hold = imme_itype;
      `ALUOP_XOR : rs2_itype_hold = imme_itype;
      `ALUOP_SRL : rs2_itype_hold = imme_itype_u;
      `ALUOP_OR  : rs2_itype_hold = imme_itype;
      `ALUOP_AND : rs2_itype_hold = imme_itype;
    endcase
  end

  // RS2
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_rs2_rdata_hold = idu_fwd_rs2_rdata;
      `OP_ITYPE_LOAD : idu_rs2_rdata_hold = imme_itype;
      `OP_ITYPE_ALU  : idu_rs2_rdata_hold = rs2_itype_hold;
      `OP_ITYPE_JUMP : idu_rs2_rdata_hold = imme_itype;
      `OP_STYPE      : idu_rs2_rdata_hold = idu_fwd_rs2_rdata;
      `OP_BTYPE      : idu_rs2_rdata_hold = idu_fwd_rs2_rdata;
      `OP_UTYPE_AUIPC: idu_rs2_rdata_hold = `WORD_SIZE'b0;
      `OP_UTYPE_LUI  : idu_rs2_rdata_hold = `WORD_SIZE'b0;
      `OP_JTYPE      : idu_rs2_rdata_hold = `WORD_SIZE'b0;
      default        : idu_rs2_rdata_hold = `WORD_SIZE'b0;
    endcase
  end

  // Byte
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_byte = 1'b0;
      `OP_ITYPE_LOAD : idu_byte = (instr[14:12] == 3'b000 |
                                   instr[14:12] == 3'b100)? 1'b1: 1'b0;
      `OP_ITYPE_ALU  : idu_byte = 1'b0;
      `OP_ITYPE_JUMP : idu_byte = 1'b0;
      `OP_STYPE      : idu_byte = (instr[14:12] == 3'b000)? 1'b1: 1'b0;
      `OP_BTYPE      : idu_byte = 1'b0;
      `OP_UTYPE_AUIPC: idu_byte = 1'b0;
      `OP_UTYPE_LUI  : idu_byte = 1'b0;
      `OP_JTYPE      : idu_byte = 1'b0;
      default        : idu_byte = 1'b0;
    endcase
  end

  // Half word
  always_comb begin
    case (imask_op)
      `OP_RTYPE      : idu_half = 1'b0;
      `OP_ITYPE_LOAD : idu_half = (instr[14:12] == 3'b001 |
                                   instr[14:12] == 3'b101)? 1'b1: 1'b0;
      `OP_ITYPE_ALU  : idu_half = 1'b0;
      `OP_ITYPE_JUMP : idu_half = 1'b0;
      `OP_STYPE      : idu_half = (instr[14:12] == 3'b001)? 1'b1: 1'b0;
      `OP_BTYPE      : idu_half = 1'b0;
      `OP_UTYPE_AUIPC: idu_half = 1'b0;
      `OP_UTYPE_LUI  : idu_half = 1'b0;
      `OP_JTYPE      : idu_half = 1'b0;
      default        : idu_half = 1'b0;
    endcase
  end

  // Unsigned for load word & load byte
  always_comb begin
    case (imask_op)
      `OP_RTYPE      : idu_unsigned = 1'b0;
      `OP_ITYPE_LOAD : idu_unsigned = (instr[14:12] == 3'b100 |
                                       instr[14:12] == 3'b101)? 1'b1: 1'b0;
      `OP_ITYPE_ALU  : idu_unsigned = 1'b0;
      `OP_ITYPE_JUMP : idu_unsigned = 1'b0;
      `OP_STYPE      : idu_unsigned = 1'b0;
      `OP_BTYPE      : idu_unsigned = 1'b0;
      `OP_UTYPE_AUIPC: idu_unsigned = 1'b0;
      `OP_UTYPE_LUI  : idu_unsigned = 1'b0;
      `OP_JTYPE      : idu_unsigned = 1'b0;
      default        : idu_unsigned = 1'b0;
    endcase
  end

  //aluop
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_aluop_hold = imask_aluop;
      `OP_ITYPE_LOAD : idu_aluop_hold = `ALUOP_ADD;
      `OP_ITYPE_ALU  : idu_aluop_hold = imask_aluop;
      `OP_ITYPE_JUMP : idu_aluop_hold = `ALUOP_ADD;
      `OP_STYPE      : idu_aluop_hold = `ALUOP_ADD;
      `OP_BTYPE      : idu_aluop_hold = imask_aluop;
      `OP_UTYPE_AUIPC: idu_aluop_hold = `ALUOP_ADD;
      `OP_UTYPE_LUI  : idu_aluop_hold = `ALUOP_ADD;
      `OP_JTYPE      : idu_aluop_hold = `ALUOP_ADD;
      default        : idu_aluop_hold = `ALUOP_ADD;
    endcase
  end

  // RD
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_rd_waddr_hold = imask_rd_id;
      `OP_ITYPE_LOAD : idu_rd_waddr_hold = imask_rd_id;
      `OP_ITYPE_ALU  : idu_rd_waddr_hold = imask_rd_id;
      `OP_ITYPE_JUMP : idu_rd_waddr_hold = imask_rd_id;
      `OP_STYPE      : idu_rd_waddr_hold = `ADDR_SIZE'b0;
      `OP_BTYPE      : idu_rd_waddr_hold = `ADDR_SIZE'b0;
      `OP_UTYPE_AUIPC: idu_rd_waddr_hold = imask_rd_id;
      `OP_UTYPE_LUI  : idu_rd_waddr_hold = imask_rd_id;
      `OP_JTYPE      : idu_rd_waddr_hold = imask_rd_id;
      default        : idu_rd_waddr_hold = `ADDR_SIZE'b0;
    endcase
  end

  // Control signals
  // ** mem2reg
  assign idu_mem2reg = (imask_op == `OP_ITYPE_LOAD)? `TRUE: `FALSE;

  // ** mem output enable
  assign idu_mem_oe = (imask_op == `OP_ITYPE_LOAD)? `ENABLE: `DISABLE;

  // ** mem web
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_mem_web = `MEM_READ;
      `OP_ITYPE_LOAD : idu_mem_web = (idu_half)?`MEM_HALF_READ: `MEM_READ;
      `OP_ITYPE_ALU  : idu_mem_web = `MEM_READ;
      `OP_ITYPE_JUMP : idu_mem_web = `MEM_READ;
      `OP_STYPE      : idu_mem_web = (idu_half)?`MEM_HALF_WRITE: `MEM_WRITE;
      `OP_BTYPE      : idu_mem_web = `MEM_READ;
      `OP_UTYPE_AUIPC: idu_mem_web = `MEM_READ;
      `OP_UTYPE_LUI  : idu_mem_web = `MEM_READ;
      `OP_JTYPE      : idu_mem_web = `MEM_READ;
      default        : idu_mem_web = `MEM_READ;
    endcase
  end

  // ** reg web
  always_comb begin
    unique case (imask_op)
      `OP_RTYPE      : idu_reg_web = `REG_WRITE;
      `OP_ITYPE_LOAD : idu_reg_web = `REG_WRITE;
      `OP_ITYPE_ALU  : idu_reg_web = `REG_WRITE;
      `OP_ITYPE_JUMP : idu_reg_web = `REG_WRITE;
      `OP_STYPE      : idu_reg_web = `REG_READ;
      `OP_BTYPE      : idu_reg_web = `REG_READ;
      `OP_UTYPE_AUIPC: idu_reg_web = `REG_WRITE;
      `OP_UTYPE_LUI  : idu_reg_web = `REG_WRITE;
      `OP_JTYPE      : idu_reg_web = `REG_WRITE;
      default        : idu_reg_web = `REG_READ;
    endcase
  end


  // Outputs pipeline assignment
  always_ff @(posedge clk, posedge rst) begin
    if(rst) begin
      idu2exu_pr_flush       <= 1'b0 ;
      idu2exu_pr_instr_valid <= `INVALID;
      idu2exu_pr_pc          <= `PC_SIZE'b0;
      idu2exu_pr_byte        <= 1'b0;
      idu2exu_pr_half        <= 1'b0;
      idu2exu_pr_unsigned    <= 1'b0;
      idu2exu_pr_imme        <= `WORD_SIZE'b0;
      idu2exu_pr_sign        <= 1'b0;
      idu2exu_pr_rs1_id      <= `REG_X0;
      idu2exu_pr_rs2_id      <= `REG_X0;
      idu2exu_pr_rs1         <= `WORD_SIZE'b0;
      idu2exu_pr_rs2         <= `WORD_SIZE'b0;
      idu2exu_pr_aluop       <= `ALUOP_SIZE'b0;
      idu2exu_pr_optype      <= 4'b0;
      idu2exu_pr_rd_addr     <= `ADDR_SIZE'b0;
      idu2exu_pr_mem_web     <= `MEM_READ;
      idu2exu_pr_mem_oe      <= `DISABLE;
      idu2exu_pr_reg_web     <= `REG_READ;
      idu2exu_pr_mem2reg     <= 1'b0;
      idu2exu_wbu_fwd_reg_sel1 <= 1'b0;
      idu2exu_wbu_fwd_reg_sel2 <= 1'b0;
    end
    else if(wrp2cpu_stall) begin
      idu2exu_pr_flush         <= idu2exu_pr_flush        ;
      idu2exu_pr_instr_valid   <= idu2exu_pr_instr_valid  ;
      idu2exu_pr_pc            <= idu2exu_pr_pc           ;
      idu2exu_pr_byte          <= idu2exu_pr_byte         ;
      idu2exu_pr_half          <= idu2exu_pr_half         ;
      idu2exu_pr_unsigned      <= idu2exu_pr_unsigned     ;
      idu2exu_pr_imme          <= idu2exu_pr_imme         ;
      idu2exu_pr_sign          <= idu2exu_pr_sign         ;
      idu2exu_pr_rs1_id        <= idu2exu_pr_rs1_id       ;
      idu2exu_pr_rs2_id        <= idu2exu_pr_rs2_id       ;
      idu2exu_pr_rs1           <= idu2exu_pr_rs1          ;
      idu2exu_pr_rs2           <= idu2exu_pr_rs2          ;
      idu2exu_pr_aluop         <= idu2exu_pr_aluop        ;
      idu2exu_pr_optype        <= idu2exu_pr_optype       ;
      idu2exu_pr_rd_addr       <= idu2exu_pr_rd_addr      ;
      idu2exu_pr_mem_web       <= idu2exu_pr_mem_web      ;
      idu2exu_pr_mem_oe        <= idu2exu_pr_mem_oe       ;
      idu2exu_pr_reg_web       <= idu2exu_pr_reg_web      ;
      idu2exu_pr_mem2reg       <= idu2exu_pr_mem2reg      ;
      idu2exu_wbu_fwd_reg_sel1 <= idu2exu_wbu_fwd_reg_sel1;
      idu2exu_wbu_fwd_reg_sel2 <= idu2exu_wbu_fwd_reg_sel2;
    end
    else begin
      idu2exu_pr_flush       <= ctrl_flush_idu;
      idu2exu_pr_instr_valid <= idu_instr_valid;
      idu2exu_pr_pc          <= ifu2idu_pr_pc;
      idu2exu_pr_byte        <= idu_byte;
      idu2exu_pr_half        <= idu_half;
      idu2exu_pr_unsigned    <= idu_unsigned;
      idu2exu_pr_imme        <= idu_imme_hold;
      idu2exu_pr_sign        <= idu_sign;
      idu2exu_pr_rs1_id      <= idu_rs1_id;
      idu2exu_pr_rs2_id      <= idu_rs2_id;
      idu2exu_pr_rs1         <= idu_rs1_rdata_hold;
      idu2exu_pr_rs2         <= idu_rs2_rdata_hold;
      idu2exu_pr_aluop       <= idu_aluop_hold;
      idu2exu_pr_optype      <= idu_optype_hold;
      idu2exu_pr_rd_addr     <= idu_rd_waddr_hold;
      idu2exu_pr_mem_web     <= idu_mem_web_hold;
      idu2exu_pr_mem_oe      <= idu_mem_oe_hold;
      idu2exu_pr_reg_web     <= idu_reg_web_hold;
      idu2exu_pr_mem2reg     <= idu_mem2reg_hold;
      idu2exu_wbu_fwd_reg_sel1 <= fwd_wbu_reg_sel1;
      idu2exu_wbu_fwd_reg_sel2 <= fwd_wbu_reg_sel2;
    end
  end

endmodule
