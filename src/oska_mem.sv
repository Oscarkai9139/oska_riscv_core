`include "oska_define.sv"


module oska_mem(input logic clk,
                input logic rst,

                // Wrapper to internal cpu
                input logic wrp2cpu_stall,

                input logic [`PC_SIZE-1:0]      exu2mem_pr_pc,
                input logic                     exu2mem_pr_byte,
                input logic                     exu2mem_pr_half,
                input logic                     exu2mem_pr_unsigned,
                input logic                     exu2mem_pr_reg_web,
                input logic [`ADDR_SIZE-1:0]    exu2mem_pr_reg_addr,
                input logic [`REG_DATASIZE-1:0] exu2mem_pr_reg_data,
                input logic                     exu2mem_pr_mem2reg,

                input  logic [`ADDR_SIZE-1:0]    exu2mem_rs1_id,
                input  logic [`ADDR_SIZE-1:0]    exu2mem_rs2_id,
                output logic                     mem2exu_fwd_reg_sel1,
                output logic                     mem2exu_fwd_reg_sel2,
                output logic [`REG_DATASIZE-1:0] mem2exu_fwd_reg_data,

                output logic                     mem2idu_fwd_reg_web,
                output logic [`ADDR_SIZE-1:0]    mem2idu_fwd_reg_addr,

                output logic [`PC_SIZE-1:0]      mem2wbu_pr_pc,
                output logic                     mem2wbu_pr_byte,
                output logic                     mem2wbu_pr_half,
                output logic                     mem2wbu_pr_unsigned,
                output logic                     mem2wbu_pr_reg_web,
                output logic [`ADDR_SIZE-1:0]    mem2wbu_pr_reg_addr,
                output logic [`REG_DATASIZE-1:0] mem2wbu_pr_reg_data,
                output logic                     mem2wbu_pr_mem2reg);

  assign mem2idu_fwd_reg_addr = exu2mem_pr_reg_addr;
  assign mem2idu_fwd_reg_web  = exu2mem_pr_reg_web;

  // Forward to execution stage (exu hazard)
  assign mem2exu_fwd_reg_data = exu2mem_pr_reg_data;
  assign mem2exu_fwd_reg_sel1 = (exu2mem_pr_reg_web  == `REG_WRITE &
                                 exu2mem_pr_reg_addr != `REG_X0    &
                                 exu2mem_rs1_id   == exu2mem_pr_reg_addr)?
                                 `TRUE: `FALSE;
  assign mem2exu_fwd_reg_sel2 = (exu2mem_pr_reg_web  == `REG_WRITE &
                                 exu2mem_pr_reg_addr != `REG_X0    &
                                 exu2mem_rs2_id   == exu2mem_pr_reg_addr)?
                                 `TRUE: `FALSE;

  always_ff @(posedge clk, posedge rst)
  begin
    if(rst) begin
      mem2wbu_pr_reg_web  <= `REG_READ;
      mem2wbu_pr_reg_addr <= `ADDR_SIZE'b0;
      mem2wbu_pr_reg_data <= `REG_DATASIZE'b0;
      mem2wbu_pr_mem2reg  <= 1'b0;
      mem2wbu_pr_pc       <= `PC_SIZE'b0;
      mem2wbu_pr_byte     <= 1'b0;
      mem2wbu_pr_half     <= 1'b0;
      mem2wbu_pr_unsigned <= 1'b0;
    end
    else if(wrp2cpu_stall) begin
      mem2wbu_pr_pc       <= mem2wbu_pr_pc      ;
      mem2wbu_pr_reg_web  <= mem2wbu_pr_reg_web ;
      mem2wbu_pr_reg_addr <= mem2wbu_pr_reg_addr;
      mem2wbu_pr_reg_data <= mem2wbu_pr_reg_data;
      mem2wbu_pr_mem2reg  <= mem2wbu_pr_mem2reg ;
      mem2wbu_pr_byte     <= mem2wbu_pr_byte    ;
      mem2wbu_pr_half     <= mem2wbu_pr_half    ;
      mem2wbu_pr_unsigned <= mem2wbu_pr_unsigned;
    end
    else begin
      mem2wbu_pr_pc       <= exu2mem_pr_pc      ;
      mem2wbu_pr_reg_web  <= exu2mem_pr_reg_web ;
      mem2wbu_pr_reg_addr <= exu2mem_pr_reg_addr;
      mem2wbu_pr_reg_data <= exu2mem_pr_reg_data;
      mem2wbu_pr_mem2reg  <= exu2mem_pr_mem2reg ;
      mem2wbu_pr_byte     <= exu2mem_pr_byte    ;
      mem2wbu_pr_half     <= exu2mem_pr_half    ;
      mem2wbu_pr_unsigned <= exu2mem_pr_unsigned;
    end
  end


endmodule
