`include "oska_define.sv"
`include "def.svh"

module oska_exu(input logic clk,
                input logic rst,

                // Wrapper to internal cpu
                input logic wrp2cpu_stall,

                // Inputs pipeline regs
                // IDU ---> EXU
                input logic                   idu2exu_pr_flush,
                input logic                   idu2exu_pr_instr_valid,
                input logic [`PC_SIZE-1:0]    idu2exu_pr_pc,
                input logic                   idu2exu_pr_byte,
                input logic                   idu2exu_pr_half,
                input logic                   idu2exu_pr_unsigned,
                input logic [`WORD_SIZE-1:0]  idu2exu_pr_imme,
                input logic                   idu2exu_pr_sign,
                input logic [`ADDR_SIZE-1:0]  idu2exu_pr_rs1_id,
                input logic [`ADDR_SIZE-1:0]  idu2exu_pr_rs2_id,
                input logic [`WORD_SIZE-1:0]  idu2exu_pr_rs1,
                input logic [`WORD_SIZE-1:0]  idu2exu_pr_rs2,
                input logic [`ALUOP_SIZE-1:0] idu2exu_pr_aluop,
                input logic [3:0]             idu2exu_pr_optype,
                input logic [`ADDR_SIZE-1:0]  idu2exu_pr_rd_addr,

                // Control Signals
                // IDU ---> EXU
                input logic [`DM_WEB_SIZE-1:0] idu2exu_pr_mem_web,
                input logic                    idu2exu_pr_mem_oe,
                input logic                    idu2exu_pr_reg_web,
                input logic                    idu2exu_pr_mem2reg,

                // Forward signals from mem
                // MEM ---> EXU
                input logic                     mem2exu_fwd_reg_sel1,
                input logic                     mem2exu_fwd_reg_sel2,
                input logic [`REG_DATASIZE-1:0] mem2exu_fwd_reg_data,
                // IDU ---> EXU
                input logic                     idu2exu_wbu_fwd_reg_sel1,
                input logic                     idu2exu_wbu_fwd_reg_sel2,
                input logic [`REG_DATASIZE-1:0] wbu2exu_fwd_reg_data,


                // Jump signals
                // EXU ---> PC
                output logic                exu2pc_jump,
                output logic [`PC_SIZE-1:0] exu2pc_jump_addr,

                // Forward signals for hazard detection
                // EXU --> MEM
                // EXU ---> WBU
                output logic [`ADDR_SIZE-1:0]    exu2mem_rs1_id,
                output logic [`ADDR_SIZE-1:0]    exu2mem_rs2_id,

                output logic [`CACHE_TYPE_BITS-1:0] cpu2dmc_type,

                // Outputs pipeline regs
                // EXU ---> MEM
                output logic [`PC_SIZE-1:0]     exu2mem_pr_pc,
                output logic                    exu2mem_pr_byte,
                output logic                    exu2mem_pr_half,
                output logic                    exu2mem_pr_unsigned,
                output logic                    exu2mem_pr_mem_oe,
                output logic [`DM_WEB_SIZE-1:0] exu2mem_pr_mem_web,
                output logic [`WORD_SIZE-1:0]   exu2mem_pr_mem_addr, //
                output logic [`WORD_SIZE-1:0]   exu2mem_pr_mem_data, //
                output logic                    exu2mem_pr_reg_web,
                output logic [`ADDR_SIZE-1:0]   exu2mem_pr_reg_addr,
                output logic [`WORD_SIZE-1:0]   exu2mem_pr_reg_data, //
                output logic                    exu2mem_pr_mem2reg);

  logic [`WORD_SIZE-1:0]  rs1;
  logic [`WORD_SIZE-1:0]  rs2;
  logic [`SHAMT_SIZE-1:0] shamt;

  logic [`WORD_SIZE-1:0]  rs1_hold;
  logic [`WORD_SIZE-1:0]  rs2_hold;

  logic blt_presult;
  logic slt_presult;
  logic slt_presult_invert;

  logic [`WORD_SIZE-1:0] alu_add_result ;
  logic [`WORD_SIZE-1:0] alu_sub_result ;
  logic [`WORD_SIZE-1:0] alu_sll_result ;
  logic                  alu_slt_result ;
  logic                  alu_sltu_result;
  logic [`WORD_SIZE-1:0] alu_xor_result ;
  logic [`WORD_SIZE-1:0] alu_srl_result ;
  logic [`WORD_SIZE-1:0] alu_sra_result ;
  logic [`WORD_SIZE-1:0] alu_or_result  ;
  logic [`WORD_SIZE-1:0] alu_and_result ;

  logic alu_beq_result;
  logic alu_bne_result;
  logic alu_blt_result;
  logic alu_bge_result;
  logic alu_bltu_result;
  logic alu_bgeu_result;

  logic [`WORD_SIZE-1:0] alu_add_addr_result;

  logic [`WORD_SIZE-1:0] alu_pc_next;
  logic [`WORD_SIZE-1:0] alu_pc_jump;
  logic [`WORD_SIZE-1:0] alu_pc_imme;

  // pipeline hold value declaration
  logic                btype_jump_result   ;
  logic [`PC_SIZE-1:0] btype_jump_addr_hold;
  logic                exu_jump_hold       ;
  logic [`PC_SIZE-1:0] exu_jump_addr_hold  ;
  logic [`WORD_SIZE-1:0] alu_result_hold   ;

  logic [`ADDR_SIZE-1:0] reg_addr_hold;
  logic [`WORD_SIZE-1:0] reg_data_hold;
  logic [`WORD_SIZE-1:0] mem_addr_hold;
  logic [`WORD_SIZE-1:0] mem_data_hold;

  assign rs1_hold = (mem2exu_fwd_reg_sel1)? mem2exu_fwd_reg_data:
                    (idu2exu_wbu_fwd_reg_sel1)?
                      wbu2exu_fwd_reg_data: idu2exu_pr_rs1;
  assign rs2_hold = (mem2exu_fwd_reg_sel2)? mem2exu_fwd_reg_data:
                    (idu2exu_wbu_fwd_reg_sel2)?
                      wbu2exu_fwd_reg_data: idu2exu_pr_rs2;

  assign exu2mem_rs1_id = idu2exu_pr_rs1_id;
  assign exu2mem_rs2_id = idu2exu_pr_rs2_id;

  // Value prepare
  assign rs1   = rs1_hold;
  assign rs2   = rs2_hold;
  assign shamt = rs2[4:0];

  assign slt_presult        = (rs1 < rs2)? 1'b1: 1'b0;
  always_comb
  begin
    case({rs1[31], rs2[31]})
      2'b00:   alu_slt_result = slt_presult;
      2'b01:   alu_slt_result = 1'b0;
      2'b10:   alu_slt_result = 1'b1;
      2'b11:   alu_slt_result = slt_presult;
    endcase
  end

  // ALU rtype itype operation
  assign alu_add_result  = rs1 + rs2;
  assign alu_sub_result  = rs1 - rs2;
  assign alu_sll_result  = rs1 << shamt;
  assign alu_sltu_result = slt_presult;
  assign alu_xor_result  = rs1 ^ rs2;
  assign alu_srl_result  = rs1 >> shamt;
  assign alu_sra_result  = (rs1[`REG_DATASIZE-1])?
    ((rs1 >> shamt) | (~({`REG_DATASIZE{1'b1}} >> shamt ))) : (rs1 >> shamt);
  assign alu_or_result   = rs1 | rs2;
  assign alu_and_result  = rs1 & rs2;

  assign alu_add_addr_result = rs1 + idu2exu_pr_imme;

  assign blt_presult = (rs1 < rs2)? 1'b1: 1'b0;

  // blt result
  always_comb
  begin
    case({rs1[31], rs2[31]})
      2'b00: alu_blt_result = blt_presult;
      2'b01: alu_blt_result =  1'b0;
      2'b10: alu_blt_result =  1'b1;
      2'b11: alu_blt_result = blt_presult;
    endcase
  end

  assign alu_bltu_result = blt_presult;

  assign bge_presult = (rs1 > rs2)? 1'b1: 1'b0;

  always_comb
  begin
    case({rs1[31], rs2[31]})
      2'b00: alu_bge_result = (alu_beq_result)? 1'b1: bge_presult;
      2'b01: alu_bge_result = (alu_beq_result)? 1'b1: 1'b1;
      2'b10: alu_bge_result = (alu_beq_result)? 1'b1: 1'b0;
      2'b11: alu_bge_result = (alu_beq_result)? 1'b1: bge_presult;
    endcase
  end

  assign alu_bgeu_result = (alu_beq_result)? 1'b1: bge_presult;

  logic [1:0] mem_addr_byte;
  logic [3:0] mem_web_byte;
  logic [3:0] mem_web_half;
  logic [3:0] mem_web_s_hold;
  logic [3:0] mem_web_l_hold;
  logic [3:0] mem_web;
  logic [3:0] mem_web_hold;

  // Mem byte enable
  assign mem_addr_byte = alu_add_addr_result[1:0];

  // mem web store byte
  always_comb
  begin
    case(mem_addr_byte)
      2'b00  : mem_web_byte = 4'b1110;
      2'b01  : mem_web_byte = 4'b1101;
      2'b10  : mem_web_byte = 4'b1011;
      2'b11  : mem_web_byte = 4'b0111;
    endcase
  end

  // mem web store half
  always_comb
  begin
    case(mem_addr_byte)
      2'b00  : mem_web_half = 4'b1100;
      2'b01  : mem_web_half = 4'b1100;
      2'b10  : mem_web_half = 4'b0011;
      2'b11  : mem_web_half = 4'b0011;
    endcase
  end


  assign mem_web_s_hold = (idu2exu_pr_byte)? mem_web_byte:
                          (idu2exu_pr_half)? mem_web_half: `MEM_WRITE;
  assign mem_web_l_hold = `MEM_READ;
  assign mem_web_hold   = (~idu2exu_pr_instr_valid)? `MEM_READ:
                          (idu2exu_pr_flush)? `MEM_READ: mem_web;

  always_comb
  begin
    case(idu2exu_pr_optype)
      `RTYPE      : mem_web = `MEM_READ;
      `ITYPE_LOAD : mem_web = mem_web_l_hold;
      `ITYPE_ALU  : mem_web = `MEM_READ;
      `ITYPE_JUMP : mem_web = `MEM_READ;
      `STYPE      : mem_web = mem_web_s_hold;
      `BTYPE      : mem_web = `MEM_READ;
      `UTYPE_AUIPC: mem_web = `MEM_READ;
      `UTYPE_LUI  : mem_web = `MEM_READ;
      `JTYPE      : mem_web = `MEM_READ;
      `ERROR_TYPE : mem_web = `MEM_READ;
      default     : mem_web = `MEM_READ;
    endcase
  end

  logic [`WORD_SIZE-1:0] mem_data_rs2_byte;
  logic [`WORD_SIZE-1:0] mem_data_rs2_half;
  logic [`WORD_SIZE-1:0] mem_data_rs2_hold;

  // Byte store data prepare
  always_comb
  begin
    case(mem_addr_byte)
      2'b00  : mem_data_rs2_byte = {24'b0, rs2[7:0]};
      2'b01  : mem_data_rs2_byte = {16'b0, rs2[7:0], 8'b0};
      2'b10  : mem_data_rs2_byte = {8'b0, rs2[7:0], 16'b0};
      2'b11  : mem_data_rs2_byte = {rs2[7:0], 24'b0};
    endcase
  end

  // Half store data prepare
  always_comb
  begin
    case(mem_addr_byte)
      2'b00: mem_data_rs2_half = {16'b0, rs2[15:0]};
      2'b01: mem_data_rs2_half = {16'b0, rs2[15:0]};
      2'b10: mem_data_rs2_half = {rs2[15:0], 16'b0};
      2'b11: mem_data_rs2_half = {rs2[15:0], 16'b0};
    endcase
  end
  assign mem_data_rs2_hold = (idu2exu_pr_byte)? mem_data_rs2_byte:
                             (idu2exu_pr_half)? mem_data_rs2_half: rs2;

  logic [`CACHE_TYPE_BITS-1:0] dmc_type_hold;
  always_comb begin
    case({idu2exu_pr_byte, idu2exu_pr_half, idu2exu_pr_unsigned})
      3'b100: dmc_type_hold = `CACHE_BYTE;
      3'b101: dmc_type_hold = `CACHE_BYTE_U;
      3'b010: dmc_type_hold = `CACHE_HWORD;
      3'b011: dmc_type_hold = `CACHE_HWORD_U;
      default: dmc_type_hold = `CACHE_WORD;
    endcase
  end
// ALU btype operation
  assign alu_beq_result  = (rs1 == rs2)? 1'b1: 1'b0;
  assign alu_bne_result  = (rs1 != rs2)? 1'b1: 1'b0;

  // AlU pc calculation jtype btype utype
  assign alu_pc_next      = {18'b0, idu2exu_pr_pc} + `WORD_SIZE'b100;
  assign alu_pc_imme      = {18'b0, idu2exu_pr_pc} + idu2exu_pr_imme;
  assign exu2pc_jump      = (idu2exu_pr_instr_valid)? exu_jump_hold: 1'b0;
  assign exu2pc_jump_addr = exu_jump_addr_hold;

  // btype jump result case
  always_comb
  begin
    case(idu2exu_pr_aluop)
      `BOP_BEQ : btype_jump_result = alu_beq_result;
      `BOP_BNE : btype_jump_result = alu_bne_result;
      `BOP_BLT : btype_jump_result = alu_blt_result;
      `BOP_BGE : btype_jump_result = alu_bge_result;
      `BOP_BLTU: btype_jump_result = alu_bltu_result;
      `BOP_BGEU: btype_jump_result = alu_bgeu_result;
      default  : btype_jump_result = 1'b0;
    endcase
  end

  // jump
  always_comb
  begin
    case(idu2exu_pr_optype)
      `RTYPE      : exu_jump_hold = 1'b0;
      `ITYPE_LOAD : exu_jump_hold = 1'b0;
      `ITYPE_ALU  : exu_jump_hold = 1'b0;
      `ITYPE_JUMP : exu_jump_hold = 1'b1;
      `STYPE      : exu_jump_hold = 1'b0;
      `BTYPE      : exu_jump_hold = btype_jump_result;
      `UTYPE_AUIPC: exu_jump_hold = 1'b0;
      `UTYPE_LUI  : exu_jump_hold = 1'b0;
      `JTYPE      : exu_jump_hold = 1'b1;
      `ERROR_TYPE : exu_jump_hold = 1'b0;
      default     : exu_jump_hold = 1'b0;
    endcase
  end

  // btype address select
  always_comb
  begin
    case(btype_jump_result)
      1'b0   : btype_jump_addr_hold = alu_pc_next[13:0];
      1'b1   : btype_jump_addr_hold = alu_pc_imme[13:0];
    endcase
  end

  // jump address select
  always_comb
  begin
    case(idu2exu_pr_optype)
      `RTYPE      : exu_jump_addr_hold = `PC_SIZE'b0;
      `ITYPE_LOAD : exu_jump_addr_hold = `PC_SIZE'b0;
      `ITYPE_ALU  : exu_jump_addr_hold = `PC_SIZE'b0;
      `ITYPE_JUMP : exu_jump_addr_hold = alu_add_result[13:0];
      `STYPE      : exu_jump_addr_hold = `PC_SIZE'b0;
      `BTYPE      : exu_jump_addr_hold = btype_jump_addr_hold;
      `UTYPE_AUIPC: exu_jump_addr_hold = `PC_SIZE'b0;
      `UTYPE_LUI  : exu_jump_addr_hold = `PC_SIZE'b0;
      `JTYPE      : exu_jump_addr_hold = alu_pc_imme[13:0];
      `ERROR_TYPE : exu_jump_addr_hold = `PC_SIZE'b0;
      default     : exu_jump_addr_hold = `PC_SIZE'b0;
    endcase
  end

  // alu final result
  always_comb
  begin
    case({idu2exu_pr_aluop, idu2exu_pr_sign})
        {`ALUOP_ADD , 1'b0} : alu_result_hold = alu_add_result ;
        {`ALUOP_SUB , 1'b1} : alu_result_hold = alu_sub_result ;
        {`ALUOP_SLL , 1'b0} : alu_result_hold = alu_sll_result ;
        {`ALUOP_SLT , 1'b0} : alu_result_hold = {31'b0, alu_slt_result} ;
        {`ALUOP_SLTU, 1'b0} : alu_result_hold = {31'b0, alu_sltu_result};
        {`ALUOP_XOR , 1'b0} : alu_result_hold = alu_xor_result ;
        {`ALUOP_SRL , 1'b0} : alu_result_hold = alu_srl_result ;
        {`ALUOP_SRA , 1'b1} : alu_result_hold = alu_sra_result ;
        {`ALUOP_OR  , 1'b0} : alu_result_hold = alu_or_result  ;
        {`ALUOP_AND , 1'b0} : alu_result_hold = alu_and_result ;
        default             : alu_result_hold = alu_add_result ;
    endcase
  end

  // mem data
  always_comb
  begin
    case(idu2exu_pr_optype)
      `RTYPE      : mem_data_hold = `WORD_SIZE'b0;
      `ITYPE_LOAD : mem_data_hold = `WORD_SIZE'b0;
      `ITYPE_ALU  : mem_data_hold = `WORD_SIZE'b0;
      `ITYPE_JUMP : mem_data_hold = `WORD_SIZE'b0;
      `STYPE      : mem_data_hold = mem_data_rs2_hold;
      `BTYPE      : mem_data_hold = `WORD_SIZE'b0;
      `UTYPE_AUIPC: mem_data_hold = `WORD_SIZE'b0;
      `UTYPE_LUI  : mem_data_hold = `WORD_SIZE'b0;
      `JTYPE      : mem_data_hold = `WORD_SIZE'b0;
      `ERROR_TYPE : mem_data_hold = `WORD_SIZE'b0;
      default     : mem_data_hold = `WORD_SIZE'b0;
    endcase
  end

  always_comb
  begin
    case(idu2exu_pr_optype)
      `RTYPE      : mem_addr_hold = `DM_ADDR_SIZE'b0;
      `ITYPE_LOAD : mem_addr_hold = alu_add_addr_result;
      `ITYPE_ALU  : mem_addr_hold = `DM_ADDR_SIZE'b0;
      `ITYPE_JUMP : mem_addr_hold = `DM_ADDR_SIZE'b0;
      `STYPE      : mem_addr_hold = alu_add_addr_result;
      `BTYPE      : mem_addr_hold = `DM_ADDR_SIZE'b0;
      `UTYPE_AUIPC: mem_addr_hold = `DM_ADDR_SIZE'b0;
      `UTYPE_LUI  : mem_addr_hold = `DM_ADDR_SIZE'b0;
      `JTYPE      : mem_addr_hold = `DM_ADDR_SIZE'b0;
      `ERROR_TYPE : mem_addr_hold = `DM_ADDR_SIZE'b0;
      default     : mem_addr_hold = `DM_ADDR_SIZE'b0;
    endcase
  end

  always_comb
  begin
    case(idu2exu_pr_optype)
      `RTYPE      : reg_data_hold = alu_result_hold;
      `ITYPE_LOAD : reg_data_hold = `WORD_SIZE'b0;
      `ITYPE_ALU  : reg_data_hold = alu_result_hold;
      `ITYPE_JUMP : reg_data_hold = alu_pc_next;
      `STYPE      : reg_data_hold = `WORD_SIZE'b0;
      `BTYPE      : reg_data_hold = `WORD_SIZE'b0;
      `UTYPE_AUIPC: reg_data_hold = alu_pc_imme;
      `UTYPE_LUI  : reg_data_hold = idu2exu_pr_imme;
      `JTYPE      : reg_data_hold = alu_pc_next;
      `ERROR_TYPE : reg_data_hold = `WORD_SIZE'b0;
      default     : reg_data_hold = `WORD_SIZE'b0;
    endcase
  end

  always_ff @(posedge clk, posedge rst)
  begin
    if(rst) begin
      exu2mem_pr_pc       <= `PC_SIZE'b0;
      exu2mem_pr_byte     <= 1'b0;
      exu2mem_pr_half     <= 1'b0;
      exu2mem_pr_unsigned <= 1'b0;
      exu2mem_pr_mem_oe   <= 1'b0;
      exu2mem_pr_mem_web  <= `MEM_READ;
      exu2mem_pr_mem_addr <= `DM_ADDR_SIZE'b0;
      exu2mem_pr_mem_data <= `DM_WORD_SIZE'b0;
      exu2mem_pr_reg_web  <= `REG_READ;
      cpu2dmc_type        <= `CACHE_WORD;
      exu2mem_pr_reg_addr <= `ADDR_SIZE'b0;
      exu2mem_pr_reg_data <= `WORD_SIZE'b0;
      exu2mem_pr_mem2reg  <= 1'b0;
    end
    else if(wrp2cpu_stall) begin
      exu2mem_pr_pc       <= exu2mem_pr_pc;
      exu2mem_pr_byte     <= exu2mem_pr_byte;
      exu2mem_pr_half     <= exu2mem_pr_half;
      exu2mem_pr_unsigned <= exu2mem_pr_unsigned;
      exu2mem_pr_mem_oe   <= exu2mem_pr_mem_oe;
      exu2mem_pr_mem_web  <= exu2mem_pr_mem_web;
      cpu2dmc_type        <= cpu2dmc_type;
      exu2mem_pr_mem_addr <= exu2mem_pr_mem_addr;
      exu2mem_pr_mem_data <= exu2mem_pr_mem_data;
      exu2mem_pr_reg_web  <= exu2mem_pr_reg_web;
      exu2mem_pr_reg_addr <= exu2mem_pr_reg_addr;
      exu2mem_pr_reg_data <= exu2mem_pr_reg_data;
      exu2mem_pr_mem2reg  <= exu2mem_pr_mem2reg;
    end
    else begin
      exu2mem_pr_pc       <= idu2exu_pr_pc;
      exu2mem_pr_byte     <= idu2exu_pr_byte;
      exu2mem_pr_half     <= idu2exu_pr_half;
      exu2mem_pr_unsigned <= idu2exu_pr_unsigned;
      exu2mem_pr_mem_oe   <= idu2exu_pr_mem_oe;
      exu2mem_pr_mem_web  <= mem_web_hold;
      cpu2dmc_type        <= dmc_type_hold;
      exu2mem_pr_mem_addr <= mem_addr_hold;
      exu2mem_pr_mem_data <= mem_data_hold;
      exu2mem_pr_reg_web  <= idu2exu_pr_reg_web;
      exu2mem_pr_reg_addr <= idu2exu_pr_rd_addr;
      exu2mem_pr_reg_data <= reg_data_hold;
      exu2mem_pr_mem2reg  <= idu2exu_pr_mem2reg;
    end
  end



endmodule
