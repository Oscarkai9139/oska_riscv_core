//================================================
// Auther:      Chen Yun-Ru (May)
// Filename:    L1C_data.sv
// Description: L1 Cache for data
// Version:     0.1
//================================================
`include "def.svh"
`include "Controller.sv"
module L1C_data(
  input clk,
  input rst,
  // Core to CPU wrapper
  input [`DATA_BITS-1:0] core_addr,
  input core_req,
  input core_write,
  input [`DATA_BITS-1:0] core_in,
  input [`CACHE_TYPE_BITS-1:0] core_type,
  // Mem to CPU wrapper
  input [`DATA_BITS-1:0] D_out,
  input D_wait,
  // CPU wrapper to core
  output logic [`DATA_BITS-1:0] core_out,
  output core_wait,
  // CPU wrapper to Mem
  output logic D_req,
  output logic [`DATA_BITS-1:0] D_addr,
  output D_write,
  output [`DATA_BITS-1:0] D_in,
  output [`CACHE_TYPE_BITS-1:0] D_type
);

  logic [`CACHE_INDEX_BITS-1:0] index;
  logic [`CACHE_DATA_BITS-1:0] DA_out;
  logic [`CACHE_DATA_BITS-1:0] DA_in;
  logic [`CACHE_WRITE_BITS-1:0] DA_write;
  logic DA_read;
  logic [`CACHE_TAG_BITS-1:0] TA_out;
  logic [`CACHE_TAG_BITS-1:0] TA_in;
  logic TA_write;
  logic TA_read;
  logic [`CACHE_LINES-1:0] valid;

  //--------------- complete this part by yourself -----------------//
  logic [`DATA_BITS-1:0] C_ADDR;
  logic C_WRITE;
  logic [`DATA_BITS-1:0] C_IN;
  logic [`CACHE_TYPE_BITS-1:0] C_TYPE;
  
  logic HIT;
  logic CA_UPDATE;
  logic CA_IN_SEL;
  logic [31:0] DA_input;
  logic [31:0] DA_output;

  //Buffer input signal from CPU
  always_comb begin
    if (core_req) begin
      C_ADDR  = core_addr;
      C_WRITE = core_write;
      C_IN    = core_in;
      C_TYPE  = core_type;
    end else begin
      C_ADDR  = C_ADDR;
      C_WRITE = C_WRITE;
      C_IN    = C_IN;
      C_TYPE  = C_TYPE;
    end
  end
  
  //Output signals connection
  //assign D_write  = C_WRITE;
  //assign D_in     = C_IN;
  //assign D_type   = C_TYPE;
  assign D_write  = core_write;
  assign D_in     = core_in;
  assign D_type   = core_type;

  //Internal signals
  assign index    = C_ADDR[9:4];
  assign HIT      = (TA_out==C_ADDR[31:10] && valid[index])?1:0;
  assign DA_read  = 1'b1;
  assign TA_in    = C_ADDR[31:10];
  assign TA_write = !CA_UPDATE;
  assign TA_read  = 1'b1;

  //Input data signal to DA
  assign DA_input = (CA_IN_SEL)?D_out:C_IN;
  always_comb begin
    case (D_addr[3:2])
      2'b00: begin
        DA_in = {96'b0, DA_input};
      end
      2'b01: begin
        DA_in = {64'b0, DA_input, 32'b0};
      end
      2'b10: begin
        DA_in = {32'b0, DA_input, 64'b0};
      end
      2'b11: begin
        DA_in = {DA_input, 96'b0};
      end
    endcase
  end

  //Output data from DA
  always_comb begin
    case (C_ADDR[3:2])
      2'b00: begin
        DA_output = DA_out[31:0];
      end
      2'b01: begin
        DA_output = DA_out[63:32];
      end
      2'b10: begin
        DA_output = DA_out[95:64];
      end
      2'b11: begin
        DA_output = DA_out[127:96];
      end
    endcase
  end
  assign core_out = DA_output;

  //Valid array
  always_ff @(posedge clk) begin
    unique if (rst) begin
      valid <= {`CACHE_LINES{1'b0}};
    end else begin
      if (CA_UPDATE) begin
        valid[index] <= 1'b1;
      end
    end
  end
  
  Controller controller(
    .clk(clk),
    .rst(rst),

    .HIT(HIT),
    .CA_UPDATE(CA_UPDATE),
    .CA_IN_SEL(CA_IN_SEL),
    .CA_WEB(DA_write),
    .C_ADDR(C_ADDR),
    .C_REQ(core_req),
    .C_WRITE(C_WRITE),
    .C_TYPE(C_TYPE),
    .D_WAIT(D_wait),
    .C_WAIT(core_wait),
    .D_REQ(D_req),
    .D_ADDR(D_addr)
  );

  data_array_wrapper DA(
    .A(index),
    .DO(DA_out),
    .DI(DA_in),
    .CK(clk),
    .WEB(DA_write),
    .OE(DA_read),
    .CS(1'b1)
  );
   
  tag_array_wrapper  TA(
    .A(index),
    .DO(TA_out),
    .DI(TA_in),
    .CK(clk),
    .WEB(TA_write),
    .OE(TA_read),
    .CS(1'b1)
  );

endmodule

